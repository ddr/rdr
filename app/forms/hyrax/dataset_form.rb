# frozen_string_literal: true

# Originally generated via
#  `rails generate hyrax:work Dataset`

# Note that this form subclasses Hyrax core's WorkForm, so must occasionally be
# updated when there are changes in Hyrax core (especially to metadata).

# Last checked for updates: Hyrax v3.1.0. See:
# https://github.com/samvera/hyrax/blob/main/app/forms/hyrax/forms/work_form.rb

module Hyrax
  class DatasetForm < Hyrax::Forms::WorkForm
    self.model_class = ::Dataset

    self.terms += %i[
      bibliographic_citation
      resource_type
      affiliation
      ark
      available
      doi
      format
      funding_agency
      grant_number
      contact
      provenance
      provenance_internal
      replaces
      is_replaced_by
      temporal
    ]

    self.required_fields = [
      :title
    ]

    def primary_terms
      required_fields + %i[creator
                           publisher
                           available
                           description
                           resource_type
                           license
                           rights_notes
                           bibliographic_citation]
    end
  end
end
