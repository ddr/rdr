# frozen_string_literal: true

class SubmissionsMailer < ApplicationMailer
  helper :rdr_submission

  def notify_followup(submission)
    @submission = submission
    subject = I18n.t('rdr.followup.email.subject')
    to = Rdr.curation_group_email
    from = Rdr.curation_group_email
    mail(to: to, from: from, subject: subject)
  end

  def notify_globus(submission)
    @submission = submission
    subject = I18n.t('rdr.globus.email.subject')
    to = [submission.submitter.email, Rdr.curation_group_email]
    from = Rdr.curation_group_email
    mail(to: to, from: from, subject: subject)
  end

  def notify_error(submission)
    @submission = submission
    subject = I18n.t('rdr.submissions.email.error.subject')
    to = Rdr.curation_group_email
    from = Rdr.curation_group_email
    mail(to: to, from: from, subject: subject)
  end

  def notify_screened_out(submission)
    @submission = submission
    subject = I18n.t('rdr.submissions.email.screened_out.subject')
    to = Rdr.curation_group_email
    from = Rdr.curation_group_email
    mail(to: to, from: from, subject: subject)
  end

  def notify_success(submission, submission_folder)
    @submission = submission
    @submission_folder = submission_folder
    subject = if @submission.using_pace == 'true'
                I18n.t('rdr.submissions.email.success_pace.subject')
              else
                I18n.t('rdr.submissions.email.success.subject')
              end
    to = submission.submitter.email
    from = Rdr.curation_group_email
    mail(to: to, from: from, subject: subject)
  end

  def notify_success_curators(submission, submission_folder)
    @submission = submission
    @submission_folder = submission_folder
    subject = if @submission.using_pace == 'true'
                I18n.t('rdr.submissions.email.success_pace.subject')
              else
                I18n.t('rdr.submissions.email.success.subject')
              end
    to = Rdr.curation_group_email
    from = Rdr.curation_group_email
    mail(to: to, from: from, subject: subject)
  end
end
