# frozen_string_literal: true

class LocalUrlsController < ApplicationController
  def show
    redirect_to resolve_id
  end

  def resolve_id
    local_url_id = params.require(:local_url_id)
    ActiveFedora::Base.find_by_ark(local_url_id)
  end

  # Patch for RDR-520: undefined method `collection_url'
  def collection_url(*args)
    hyrax.collection_url(args)
  end
end
