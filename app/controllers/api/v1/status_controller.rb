# frozen_string_literal: true

module API::V1
  class StatusController < BaseController
    def index
      render json: ::Status.new
    end
  end
end
