# frozen_string_literal: true

class ExportFilesController < ApplicationController
  def verify_export_login
    @login = new_params[:login]
    if @login.present?
      store_location_for(:user, hyrax_dataset_path)
      authenticate_user!
    else
      flash[:alert] = I18n.t('rdr.batch_export.verify.help')
      render 'export_files/unauthenticated'
    end
  end

  def verify_export_recaptcha
    if verify_recaptcha
      session[:verified_recaptcha] = true
      flash[:notice] = I18n.t('rdr.batch_export.verify.recaptcha_success')
    else
      session.delete(:verified_recaptcha)
      flash[:error] = I18n.t('rdr.batch_export.verify.recaptcha_fail')
    end
    redirect_to hyrax_dataset_path and return
  end

  private

  def new_params
    params.permit(:id, :login)
  end
end
