# frozen_string_literal: true

module Hyrax::CollectionPresentable
  extend ActiveSupport::Concern

  def toplevel_work_id
    (id if top_level) || ancestor_trail_ids(solr_document).last || nil
  end

  def presenter_factory_arguments
    [current_ability, request]
  end

  # Modeled on '#member_of_collection_presenters' in Hyrax::WorkShowPresenter
  # Get the presenters for all the collections in which the top-level work is a
  # member, no matter what collection type or how deeply nested the current work is.
  # Limit to collections the current user can access.
  #
  # @return [Array<CollectionPresenter>] presenters
  def toplevel_work_collection_presenters
    return [] if toplevel_work_id.blank?
    PresenterFactory.build_for(ids: toplevel_authorized_collections,
                                presenter_class: collection_presenter_class,
                                presenter_args: presenter_factory_arguments)
  end

  # Get the presenters for the collections in which the top-level work is a member
  # Collection must be the custom RDR type "Collection" (omit admin sets, user collections, etc.)
  #
  # @return [Array<CollectionPresenter>] presenters
  def toplevel_rdr_collection_presenters
    toplevel_work_collection_presenters.select { |p| p.collection_type.machine_id == 'collection' }
  end

  # Modeled on '#member_of_authorized_parent_collections' in Hyrax::WorkShowPresenter
  def toplevel_authorized_collections
    Hyrax::CollectionMemberService.run(::SolrDocument.find(toplevel_work_id), current_ability).map(&:id)
  end

  def ancestor_trail
    docs = ancestor_trail_ids(solr_document).map { |id| ::SolrDocument.find(id) }
    docs.reverse
  end

  # Recursively assemble an array of this work's ancestor work ids, provided it
    # has ancestor works, and neither it nor any of its ancestors has multiple parents.
  def ancestor_trail_ids(document, ancestors = [])
    return ancestors if document.in_works_ids.blank?
    return [] if document.in_works_ids.count > 1

    ancestors.concat document.in_works_ids
    ancestor_trail_ids(::SolrDocument.find(document.in_works_ids.first), ancestors)
  end
end