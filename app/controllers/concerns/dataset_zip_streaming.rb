# frozen_string_literal: true

require 'zip_tricks'

module DatasetZipStreaming
  extend ActiveSupport::Concern

  included do
    include ZipTricks::RailsStreaming
  end

  BUFSIZE = 16_384

  # This method is a callback of Hyrax::WorksControllerBehavior#show
  def additional_response_formats(wants)
    wants.tar { stream_archive(:tar) }
    wants.tgz { stream_archive(:tgz) }
    wants.zip { stream_archive(:zip) }
  end

  def stream_archive(format)
    if !presenter.small_dataset?
      render(status: :forbidden, body: nil)
    elsif can_export_zip?
      dataset_id = params[:id]
      @export = Globus::Export.find_by!(dataset_id: dataset_id)
      # Set a session cookie so the client can know the zip
      # has begun assembling
      cookies["duke_rdr_export_begin_#{dataset_id}"] = DateTime.now
      send "stream_#{format}"
    else
      redirect_to main_app.verify_exportable_path
    end
  end

  # This is reasonably responsive when not compressed
  def stream_tar(compressed: false)
    response.headers['X-Accel-Buffering'] = 'no'
    response.sending_file = true
    root, path = File.split(@export.export_path)
    tar_opts = compressed ? '-zc' : '-c'

    self.response_body = Enumerator.new do |tarball|
      IO.popen("tar #{tar_opts} #{path}", chdir: root) do |io|
        while buf = io.read(BUFSIZE)
          tarball << buf
        end
      end
    end
  end

  def stream_tgz
    stream_tar(compressed: true)
  end

  def stream_zip
    zip_tricks_stream do |zip|
      FileUtils.cd(@export.export_path) do
        @export.relative_file_paths.each do |path|
          zip.write_deflated_file(path) do |sink|
            IO.copy_stream(path, sink)
          end
        end
      end
    end
  end

  def can_export_zip?
    trusted_ip? || current_user || session[:verified_recaptcha]
  end

  def trusted_ip?
    %w[127.0.0.1 ::1].include?(request.remote_ip)
  end
end
