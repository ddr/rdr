# frozen_string_literal: true
module Hyrax
    class RdrFileSetsController < Hyrax::FileSetsController
        self.show_presenter = Hyrax::RdrFileSetPresenter
    end
end