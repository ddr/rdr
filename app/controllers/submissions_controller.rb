# frozen_string_literal: true

class SubmissionsController < ApplicationController
  load_and_authorize_resource

  def new
    @agreement_display = ContentBlock.for(:agreement)
  end

  def create
    @submission.submitter = current_user
    @submission.user_agent = request.user_agent

    if @submission.valid?

      if @submission.followup?
        SubmissionsMailer.notify_followup(@submission).deliver_now
        render :followup

      elsif @submission.passed_screening?
        deposit_agreement_path = Submissions::DocumentDepositAgreement.call(@submission.submitter)
        deposit_instructions_path = Submissions::CreateDepositInstructions.call
        manifest_path = Submissions::CreateManifest.call(@submission)
        adding_collaborator = if @submission.using_pace? || @submission.using_globus?
                                nil
                              else
                                true
                              end
        submission_folder = Submissions::InitializeSubmissionFolder.call(@submission.submitter,
                                                                         deposit_agreement: deposit_agreement_path,
                                                                         deposit_instructions: deposit_instructions_path,
                                                                         manifest: manifest_path,
                                                                         collaborator: adding_collaborator)
        if @submission.using_globus?
          SubmissionsMailer.notify_globus(@submission).deliver_now
          render :globus
        else
          SubmissionsMailer.notify_success(@submission, submission_folder).deliver_now
          SubmissionsMailer.notify_success_curators(@submission, submission_folder).deliver_now
          render :create
        end
      else
        SubmissionsMailer.notify_screened_out(@submission).deliver_now
        render :screened_out
      end

    else
      SubmissionsMailer.notify_error(@submission).deliver_now
      render :error
    end
  end

  def create_params
    params.require(:submission).permit(*Submission::ATTRIBUTES)
  end
end
