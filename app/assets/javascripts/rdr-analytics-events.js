// Extends Hyrax analytics event-tracking (note: supports either GA or Matomo via config).
// This adds RDR-specific events, but only uses the Matomo syntax for simplicity.

// See:
// https://github.com/samvera/hyrax/blob/hyrax-v3.5.0/app/assets/javascripts/hyrax/analytics_events.js

// Last checked for updates: Hyrax 3.5.0

// DOWNLOADS & EXPORTS: INDIVIDUAL FILES
// =====================================
// Track download link clicks whether on work or fileset show page.
// NOTE: DUL uses file_download_<fileset_id> for the download link instead of just file_download
// because there's usually more than one on a page.
$(document).on('click', '[id^="file_download"]', function(e) {
  // DUL events
  _paq.push(['trackEvent', 'Files', 'Downloaded', $(this).data('label')]);

  // Hyrax + Matomo events
  _paq.push(['trackEvent', 'file-set', 'file-set-download', $(this).data('label')]);
  _paq.push(['trackEvent', 'file-set-in-work', 'file-set-in-work-download', $(this).data('work-id')]);
  $(this).data('collection-ids').forEach(function (collection) {
    _paq.push(['trackEvent', 'file-set-in-collection', 'file-set-in-collection-download', collection]);
    _paq.push(['trackEvent', 'work-in-collection', 'work-in-collection-download', collection]);
  });
});

// DOWNLOADS & EXPORTS: FULL DATASETS
// =====================================
// User clicked Export Files from a work show page
$(document).on('click', '#bulk_export_begin', function(e) {
  _paq.push(['trackEvent', 'Bulk Export', 'Bulk Export Initiated (' + $(this).data('format') + ')', $(this).data('label')]);
});

$(document).on('click', '.bulk_export_dropdown_format', function(e) {
  _paq.push(['trackEvent', 'Bulk Export', 'Bulk Export Initiated (' + $(this).data('format') + ')', $(this).data('label')]);

  // Hyrax + Matomo events
  _paq.push(['trackEvent', 'file-set-in-work', 'file-set-in-work-download', $(this).data('work-id')]);
  $(this).data('collection-ids').forEach(function (collection) {
    _paq.push(['trackEvent', 'file-set-in-collection', 'file-set-in-collection-download', collection]);
    _paq.push(['trackEvent', 'work-in-collection', 'work-in-collection-download', collection]);
  });
});

// User clicked Download Data from Globus from top of a work show page
$(document).on('click', '#globus_download', function(e) {
  _paq.push(['trackEvent', 'Bulk Export', 'Globus Download', $(this).data('label')]);

  // Hyrax + Matomo events
  _paq.push(['trackEvent', 'file-set-in-work', 'file-set-in-work-download', $(this).data('work-id')]);
  $(this).data('collection-ids').forEach(function (collection) {
    _paq.push(['trackEvent', 'file-set-in-collection', 'file-set-in-collection-download', collection]);
    _paq.push(['trackEvent', 'work-in-collection', 'work-in-collection-download', collection]);
  });
});

// User clicked Get File from Globus (for large file)
$(document).on('click', '[id^="globus_download_"]', function(e) {
  _paq.push(['trackEvent', 'Files', 'Globus Download', $(this).data('label')]);

  // Hyrax + Matomo events
  _paq.push(['trackEvent', 'file-set', 'file-set-download', $(this).data('label')]);
  _paq.push(['trackEvent', 'file-set-in-work', 'file-set-in-work-download', $(this).data('work-id')]);
  $(this).data('collection-ids').forEach(function (collection) {
    _paq.push(['trackEvent', 'file-set-in-collection', 'file-set-in-collection-download', collection]);
    _paq.push(['trackEvent', 'work-in-collection', 'work-in-collection-download', collection]);
  });
});

// User clicked More About Globus from a work show page
$(document).on('click', '#globus_more', function(e) {
  _paq.push(['trackEvent', 'Bulk Export', 'More About Globus', $(this).data('label')]);
});
