/* For the /help page, support linking to a sub-section tab (e.g., Globus) */
/* Also support linking to individual FAQs in the accordion either from an */
/* external URL or from other content within the FAQ page.                 */

function openTabAndAccordionToHash(){
  if(location.pathname == "/help" && location.hash != null && location.hash != ""){
    // Open the tab if one corresponds to the hash
    $('#navlist a[href="' + location.hash + '"]').tab('show');

    // Open the FAQ if it corresponds to the hash
    $('#faq .collapse').removeClass('in');
    $('#faq ' + location.hash + '.collapse').collapse('show');
    $('#faq ' + location.hash + '.collapse').addClass('in');
  }
}

$(document).ready(function () {
  openTabAndAccordionToHash();

  /* Listen for hash changes, in cases where we link to an */
  /* individual FAQ from another individual FAQ... */
  $(window).on('hashchange', function() {
    openTabAndAccordionToHash();
  });

  /* Expand/collapse all button */
  $(document).on("click","#expand-all-faqs:not('.all-expanded')", function(e) {
    e.preventDefault();
    $(this).html('collapse all');
    $('#faq .collapse').collapse('show');
    $('#faq .collapse').addClass('in');
    $(this).addClass('all-expanded');
  });

  $(document).on("click","#expand-all-faqs.all-expanded", function(e) {
    e.preventDefault();
    $(this).html('expand all');
    $('#faq .collapse').collapse('hide');
    $('#faq .collapse').removeClass('in');
    $(this).removeClass('all-expanded');
  });
});
