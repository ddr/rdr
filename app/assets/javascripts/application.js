// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//=
//
// Required by Blacklight
//= require jquery3
//= require rails-ujs
//= require jquery-ui
//= require popper
//= require twitter/typeahead
//= require bootstrap
//= require jquery.dataTables
//= require dataTables.bootstrap4
//= require blacklight/blacklight
//= require blacklight_gallery
//= require 'blacklight_range_limit_custom'
//= require 'blacklight_range_limit'

//= require_tree .
//= stub rdr-submissions.js
//= require hyrax
//= require commonmark.min.js
//= require rdr-show.js
//= require rdr-help.js
//= require rdr-analytics-events.js
