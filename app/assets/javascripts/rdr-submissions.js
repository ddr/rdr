// **** Script for submissions ****

// disable back button
$(document).ready(function() {
  window.history.pushState(null, "", window.location.href);        
  window.onpopstate = function() {
    window.history.pushState(null, "", window.location.href);
  };
});

// force reload on back button
(function () {
  window.onpageshow = function(event) {
    if (event.persisted) {
      window.location.reload();
    }
  };
})();


var submitForm = false;
// var skipCalculator = true; // commented out for removal of cost calculator (RDR-649)
var skipSize = false;
var currentTab = 0; // First tab is 0
// var totalTabs = 6; // updated from '7' via removal of cost calculator (RDR-649)
var totalTabs = 6; // updated back to 6 to account for removal of DOI step (RK-39)

$("#submission_submit_button").hide(); // hide the submit button
$("#review_button").hide(); // hide the review button
showTab(currentTab); // Display the current tab


// get variables passed from rails
var t7_exclude = document.getElementById("submission-values").getAttribute("data-t7-not-use");
var t2_small_data_size = document.getElementById("submission-values").getAttribute("data-t2-small-data-size");
var t2_medium_data_size = document.getElementById("submission-values").getAttribute("data-t2-medium-data-size");
var t2_large_data_size = document.getElementById("submission-values").getAttribute("data-t2-large-data-size");

// the following vars are commented out for removal of cost calculator (RDR-649) -- make sure to read through comments in this file upon restoring functionality!
// var storage_multiplier_l1 = document.getElementById("submission-values").getAttribute("data-level1-multiplier");
// var storage_multiplier_l2 = document.getElementById("submission-values").getAttribute("data-level2-multiplier");
// var storage_multiplier_l3 = document.getElementById("submission-values").getAttribute("data-level3-multiplier");
// var storage_multiplier_l4 = document.getElementById("submission-values").getAttribute("data-level4-multiplier");
// var storage_multiplier_l5 = document.getElementById("submission-values").getAttribute("data-level5-multiplier");

// var storage_text_l1 = document.getElementById("submission-values").getAttribute("data-storage-cost-text-l1");
// var storage_text_l2 = document.getElementById("submission-values").getAttribute("data-storage-cost-text-l2");
// var storage_text_l3 = document.getElementById("submission-values").getAttribute("data-storage-cost-text-l3");
// var storage_text_l4 = document.getElementById("submission-values").getAttribute("data-storage-cost-text-l4");
// var storage_text_l5 = document.getElementById("submission-values").getAttribute("data-storage-cost-text-l5");

// var storage_description_l1 = document.getElementById("submission-values").getAttribute("data-storage-cost-description-l1");
// var storage_description_l2 = document.getElementById("submission-values").getAttribute("data-storage-cost-description-l2");
// var storage_description_l3 = document.getElementById("submission-values").getAttribute("data-storage-cost-description-l3");
// var storage_description_l4 = document.getElementById("submission-values").getAttribute("data-storage-cost-description-l4");
// var storage_description_l5 = document.getElementById("submission-values").getAttribute("data-storage-cost-description-l5");


// slow down double clicking
$('#nextBtn, #prevBtn').click(function() {
  var mySelf = $(this);
  mySelf.addClass('disabled');
  setTimeout(function(){
    mySelf.removeClass('disabled');
  }, 350);
});


$("#review_button").click(function() {
  // account for 'washout' conditions when clicking review button
  if (currentTab == 1 && $("input[name='submission[screening_pii]']:checked").val() == "yes") {
    resetTextFields();
    resetAgreement();
    submitForm = true;
  } 
  if (currentTab == 2 && $("input[name='submission[screening_data_size]']:checked").val() == t2_large_data_size) {
    resetTextFields();
    resetAgreement()
    submitForm = true;
  } 

  updateProgress(7); // changed from '8' to '7' for removal of DOI
  nextPrev(2);
  $("#review_button").hide();

});

function showTab(n) {
  var theFormTab = document.getElementsByClassName("form-tab");
  
  //theFormTab[n].style.display = "block";
  $(theFormTab[n]).show();

  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "block";
  }

  // Change 'next' to 'submit' on last tab
  if (currentTab == totalTabs) {
    $("#nextBtn").hide();
    $("#submission_submit_button").show();
  }

  if (currentTab == 0) {
    updateProgress(1);
  }

  if (currentTab == 1) {
    updateProgress(2);
  }

  if (currentTab == 2) {
    updateProgress(3);
  }

  // commented out for removal of cost calculator (RDR-649)
  // if (currentTab == 3) {

  //   // Cost calculator!
    
  //   $("#submission_cost_calculator_data_size").change(function(){
  //      update_cost_estimate();
  //   });

  //   $("#submission_cost_calculator").change(function(){
  //      update_cost_estimate();
  //   });

  //   update_cost_estimate = function() {
      
  //     $("#cost-calculator-agreement-1").prop('checked', false); // uncheck the 'I agree' button
  //     $("#cost-calculator-agreement-1").parent().removeClass("active");

  //     var multiplier = storage_multiplier_l1; //annually (default)
  //     var explanatoryText = storage_description_l1;
  //     var totalCost = 0;
  //     var freeStorageCost = 0;

  //     if ( $("#submission_cost_calculator").val() == storage_text_l2 ) {
  //       multiplier = storage_multiplier_l2; //five
  //       explanatoryText = storage_description_l2;
  //     }

  //     if ( $("#submission_cost_calculator").val() == storage_text_l3 ) {
  //       multiplier = storage_multiplier_l3; //seven
  //       explanatoryText = storage_description_l3;
  //     }

  //     if ( $("#submission_cost_calculator").val() == storage_text_l4 ) {
  //       multiplier = storage_multiplier_l4; //ten
  //       explanatoryText = storage_description_l4;
  //     }

  //     if ( $("#submission_cost_calculator").val() == storage_text_l5 ) {
  //       multiplier = storage_multiplier_l5; //perpetual
  //       explanatoryText = storage_description_l5;
  //     }

  //     // get raw cost
  //     totalCost = $("#submission_cost_calculator_data_size").val() * multiplier;

  //     // subtract 100gb for free
  //     freeStorageCost = 100 * multiplier;
  //     totalCost = totalCost - freeStorageCost;

  //     if (totalCost < 0) {
  //       totalCost = 0;
  //     }

  //     // round to 2 decimal places and insert commas
  //     formattedTotalCost = Number(totalCost).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  //     // add a $ sign
  //     formattedTotalCost = "$" + formattedTotalCost;

  //     // update output
  //     $("#submission_cost_calculator_data_size_output").val(formattedTotalCost);
  //     $("#cost-estimate-text").text(explanatoryText);

  //   }

  // }

  if (currentTab == 3) { // updated currentTab from '4' to '3' for removal of cost calculator
    updateProgress(4);
    $("#prevBtn").click(function(){
      // console.log('clicked');
      $("#deposit-agreement-1").prop( "checked", false ); // uncheck the 'I agree' button
      $("#deposit-agreement-1").parent().removeClass("active");
    });
  }

  if (currentTab == 4) { // updated currentTab from '5' to '4' for removal of cost calculator
    updateProgress(5);
    // set error display if needed
    if ($("#t4 .text-area-empty").length > 0) { // updated #t5 to #t4 for removal of cost calculator
      addError();
    } else {
      removeError();
    }
  }

  // Removing DOI step via RK-39 -- 2/22/2024
  // if (currentTab == 5) { // updated currentTab from '6' to '5' for removal of cost calculator
  //   updateProgress(6);
  //   // set error display if needed
  //   if ( $("input[name='submission[doi_exists]']:checked").val() == "yes" && $("#submission_doi").val() == '' ) {
  //     addError();
  //     document.getElementById("submission_doi").classList.add("text-area-empty");
  //   } else {
  //     removeError();
  //     document.getElementById("submission_doi").classList.remove("text-area-empty");
  //   }
  // }

  if (currentTab == 5) { // updated currentTab from '7' to '6' for removal of cost calculator, then updated from '6' to '5' for removal of DOI
    updateProgress(6); // changed from '7' to '6' for removal of DOI
    // set error display if needed
    if ( $("input[name='submission[using_cc0]']:checked").val() == "will not use cc0" && $("#t6 select").val() == '' ) { // updated #t7 to #t6 for removal of cost calculator
      addError();
      document.getElementById("submission_license").classList.add("text-area-empty");
    } else {
      removeError();
      document.getElementById("submission_license").classList.remove("text-area-empty");
    }
  }

  if (currentTab == 6) { // this is the 'review' step
    updateProgress(7); // changed from '8' to '7' for removal of DOI
    $("#review_button").hide();

    // data size
    $( "#data-size_review_field1 p" ).text( $("input[name='submission[screening_data_size]']:checked").val() );

    // sumbission info (hide unfilled, non-required fields)
    $( "#submission_review_field1 dd").text( $("#submission_title").val() );
    $( "#submission_review_field2 dd").text( $("#submission_creator").val() );
    if ( $("#submission_contributor").val() != '' ){
      $( "#submission_review_field3 dd").text( $("#submission_contributor").val() );
      $( "#submission_review_field3").show();
    } else {
      $( "#submission_review_field3").hide();
    }
    if ( $("#submission_affiliation").val() != '' ){
      $( "#submission_review_field4 dd").text( $("#submission_affiliation").val() );
      $( "#submission_review_field4").show();
    } else {
      $( "#submission_review_field4").hide();
    }
    $( "#submission_review_field5 dd").text( $("#submission_contact").val() );
    $( "#submission_review_field6 dd").text( $("#submission_description").val() );
    $( "#submission_review_field7 dd").text( $("#submission_subject").val() );
    if ( $("#submission_based_near").val() != '' ){
      $( "#submission_review_field8 dd").text( $("#submission_based_near").val() );
      $( "#submission_review_field8").show();
    } else {
      $( "#submission_review_field8").hide();
    }
    if ( $("#submission_temporal").val() != '' ){
      $( "#submission_review_field9 dd").text( $("#submission_temporal").val() );
      $( "#submission_review_field9").show();
    } else {
      $( "#submission_review_field9").hide();
    }
    if ( $("#submission_language").val() != '' ){
      $( "#submission_review_field10 dd").text( $("#submission_language").val() );
      $( "#submission_review_field10").show();
    } else {
      $( "#submission_review_field10").hide();
    }
    if ( $("#submission_format").val() != '' ){
      $( "#submission_review_field11 dd").text( $("#submission_format").val() );
      $( "#submission_review_field11").show();
    } else {
      $( "#submission_review_field11").hide();
    }
    if ( $("#submission_related_url").val() != '' ){
      $( "#submission_review_field12 dd").text( $("#submission_related_url").val() );
      $( "#submission_review_field12").show();
    } else {
      $( "#submission_review_field12").hide();
    }
    if ( $("#submission_funding_agency").val() != '' ){
      $( "#submission_review_field13 dd").text( $("#submission_funding_agency").val() );
      $( "#submission_review_field13").show();
    } else {
      $( "#submission_review_field13").hide();
    }
    if ( $("#submission_grant_number").val() != '' ){
      $( "#submission_review_field14 dd").text( $("#submission_grant_number").val() );
      $( "#submission_review_field14").show();
    } else {
      $( "#submission_review_field14").hide();
    }

    // Removing DOI step via RK-39 -- 2/22/2024
    // DOI
    // $( "#doi_review_field1 dd").text( $("input[name='submission[doi_exists]']:checked").val() );
    // if ( $("input[name='submission[doi_exists]']:checked").val() == 'yes' ) {
    //   $( "#doi_review_field2").show();
    //   $( "#doi_review_field2 dd").text( $( "#submission_doi" ).val() );
    // } else {
    //   $( "#doi_review_field2").hide();
    // }

    // creative commons
    $( "#cc_review_field1 dd").text( $("input[name='submission[using_cc0]']:checked").val() );
    if ($("input[name='submission[using_cc0]']:checked").val() == 'will not use cc0') {
      $( "#cc_review_field2 dd").text( $( "#submission_license option:selected" ).text() );
      $( "#cc_review_field2").show();
    } else {
      $( "#cc_review_field2").hide();
    }

    // edit buttons
    $("#restricted-info-edit").click(function(){
      updateProgress(2);
      currentTab = 1;
      nextPrev(-2);
      $("#review_button").show();
    });

    $("#deposit-size-edit").click(function(){
      updateProgress(3);
      currentTab = 2;
      nextPrev(-2);
      $("#review_button").show();
    });

    $("#deposit-agreement-edit").click(function(){
      updateProgress(4);
      currentTab = 3;
      nextPrev(-2);
      $("#review_button").show();
    });

    $("#submission-info-edit").click(function(){
      updateProgress(5);
      currentTab = 4;
      nextPrev(-2);
      $("#review_button").show();
    });

    // Removing DOI step via RK-39 -- 2/22/2024
    // $("#doi-info-edit").click(function(){
    //   updateProgress(6);
    //   currentTab = 5;
    //   nextPrev(-2);
    //   $("#review_button").show();
    // });

    $("#cc-info-edit").click(function(){
      updateProgress(6); // changed from '7' to '6' for removal of DOI
      currentTab = 5; // changed from '6' to '5' for removal of DOI
      nextPrev(-2);
      $("#review_button").show();
    });

  }
}

function nextPrev(n) {

  if (n == 1 && !validateForm()) {
    $("#review_button").hide();
    return false;
  }

  if ((n == 1 && submitForm == true) || (n == 2 && submitForm == true) || (n == 1 && currentTab == totalTabs)) {

    // washout on PII
    if ( $("input[name='submission[screening_pii]']:checked").val() == "yes" ) {
      resetTextFields();
      resetAgreement();
    }

    // washout on large data size
    if ( $("input[name='submission[screening_data_size]']:checked").val() == t2_large_data_size ) {
      resetTextFields();
      resetAgreement();
    }

    // disable submit button to prevent weird things from happening
    document.getElementById("nextBtn").classList.add("disabled");
    document.getElementById("nextBtn").disabled = true;

    // display modal about submission having happened
    $('#submitModal').modal({backdrop: 'static', keyboard: false});


    // submit the form
    document.getElementById("new_submission").submit();

    resetRadioButtons();
    resetGlobus();
    resetTextFields();

    return false;
  }
  
  // review button click
  if (n == 2 ) {
    clearAllContent();
    currentTab = 6; // updated from '7' to '6' for removal of DOI
  }

  // advance the form
  if (n == 1) {
    clearAllContent();
    $("#review_button").hide();
    currentTab += 1;
  }

  // if previous button (or coming from review step)
  if (n < 0) {
    submitForm = false;
    removeError();
    $("#nextBtn").show();
    $("#submission_submit_button").hide();
    $("#review_button").hide();
    clearAllContent();
    document.getElementById("t" + currentTab).focus(); // focus on content

    // account for both size AND cost calculator on prev (using PACE)
    // if (currentTab == 3 && skipSize == true) { // updated currentTab from '4' to '3' via removal of cost calculator
      // currentTab -= 1; // updated currentTab -=2 to -=1 via removal of cost calculator
    // }

    // commented out to account for removal of cost calculator (RDR-649)
    // account for skipping cost calculator on prev
    // if (currentTab == 4 && skipCalculator == true) {
    //   currentTab -= 1;
    // }

  }

  // account for skipping both size AND cost calculator on next (using PACE)
  // if (n != -1 && currentTab == 1 && skipSize == true) {
    // currentTab += 1; // updated currentTab +=2 to +=1 via removal of cost calculator
  // }
  
  // commented out to account for removal of cost calculator (RDR-649)
  // account for skipping cost calculator on next
  // if (n != -1 && currentTab == 2 && skipCalculator == true) {
  //   currentTab += 1;
  // }

  // normal prev button
  if (n == -1) {
    currentTab = currentTab + n;
  }
  
  showTab(currentTab);

  // debug
  // console.log('current tab: ' + currentTab);
  // console.log('+++');
}


function validateForm() {
  var myFormTab,
    myInput,
    myTextArea,
    i,
    j,
    valid = true;

  myFormTab = document.getElementsByClassName("form-tab");
  myInput = myFormTab[currentTab].getElementsByTagName("input");
  myTextArea = myFormTab[currentTab].getElementsByTagName("textarea");

  // Check every input field in the current tab:
  for (i = 0; i < myInput.length; i++) {
    // If it's a radio button
    if (myInput[i].type == "radio") {
      if (myInput[i].checked == true) {
        // Submit early for certain responses

        if (currentTab == 0) {
          document.getElementById("nextBtn").classList.remove("disabled");
        }

        if (currentTab == 1 && myInput[i].value == "yes") {
          submitForm = true;
        }

        // if (currentTab == 1) {

          // commented out to account for removal of PACE workflow (RDR-649)

          // if ( $("#submission_screening_pace").val() != "" ) {
          //   if ( $("#submission_screening_pace")[0].validity.valid == false) {
          //     $("#submission_screening_pace").addClass("text-area-empty");
          //     $("#submission_screening_pace_helper_text").removeClass("d-none");
          //     addError();
          //     valid = false;
          //     break;
          //   } else {
          //     $("#submission_screening_pace").removeClass("text-area-empty");
          //     $("#submission_screening_pace_helper_text").addClass("d-none");
          //     $("#submission_using_pace").val('true');
          //     $("#submission_using_globus").val('false'); // make sure we're not using globus
          //     $("#data-size-1").prop("checked", true); // set size to use box workflow
          //     skipSize = true; // skip size and calculator
          //     removeError();
          //   }
          // } else {
          //   $("#submission_screening_pace").removeClass("text-area-empty");
          //   $("#submission_screening_pace_helper_text").addClass("d-none");
          //   $("#submission_using_pace").val('false');
          //   $("#data-size-1").prop('checked', false); // reset size
          //   skipSize = false;
          //   removeError();
          // }
        // }


        if (currentTab == 2 && myInput[i].value == t2_small_data_size) {
          $("#submission_using_globus").val('false');
          // skipCalculator = true; // commented out to account for removing cost calculator (RDR-649)
          // resetCostCalculator(); // commented out to account for removing cost calculator (RDR-649)
          $("#submission_using_large_files").val('false');
        }

        if (currentTab == 2 && myInput[i].value == t2_medium_data_size) {
          $("#submission_using_globus").val('true');
          // skipCalculator = true; // commented out to account for removing cost calculator (RDR-649)
          // resetCostCalculator(); // commented out to account for removing cost calculator (RDR-649)
          $("#submission_using_large_files").val('false');
        }

        if (currentTab == 2 && myInput[i].value == t2_large_data_size) { // go to tab 3
          // skipCalculator = false; // commented out to account for removing cost calculator (RDR-649)
          // $("#submission_using_globus").val('true'); // commented out to account for removing cost calculator (RDR-649)
          
          // added to account for removing cost calculator (RDR-649)
          $("#submission_using_large_files").val('true');
          submitForm = true;
        }


      // commented out for removal of cost calculator (RDR-649)
        // if (currentTab == 3) {

        //   fundCodeVal = $("#submission_cost_calculator_fund_code").val();
        //   dataSizeVal = $("#submission_cost_calculator_data_size").val();
        //   costCalculatorVal = $("#submission_cost_calculator").val();
        //   checkingCostVal = $("#submission_cost_calculator_data_size_output").val();

        //   // console.log ('costCalculatorVal: ');
        //   // console.log(costCalculatorVal);

        //   if (!fundCodeVal || !dataSizeVal || !costCalculatorVal) {
            
        //     if (!dataSizeVal) {
        //       $("#submission_cost_calculator_data_size").addClass("text-area-empty");
        //     } else {
        //       $("#submission_cost_calculator_data_size").removeClass("text-area-empty");
        //     }

        //     if (costCalculatorVal == '') {
        //       $("#submission_cost_calculator").addClass("text-area-empty");
        //     } else {
        //       $("#submission_cost_calculator").removeClass("text-area-empty");
        //     }

        //     if (!fundCodeVal) {
        //       $("#submission_cost_calculator_fund_code").addClass("text-area-empty");
        //     } else {
        //       $("#submission_cost_calculator_fund_code").removeClass("text-area-empty");
        //     }

        //     addError();
        //     valid = false;
        //     break;

        //   }

        //   if (fundCodeVal && dataSizeVal && costCalculatorVal) {
        //     removeError();
        //     $("#submission_cost_calculator_fund_code").removeClass("text-area-empty");
        //     $("#submission_cost_calculator_data_size").removeClass("text-area-empty");
        //     $("#submission_cost_calculator").removeClass("text-area-empty");
        //   }


        //   if (myInput[i].value == "I agree") { // don't allow 'I agree' without values
            
        //     if ( !checkingCostVal ) {
        //       addError();
        //       valid = false;
        //       break;
        //     }
        //   }

        // }


        

        // Manually check text field inputs

        // Removing DOI step via RK-39 -- 2/22/2024
        // if (currentTab == 5 && myInput[i].value == "yes") { // updated currentTab from '6' to '5' for removal of cost calculator
        //   if (document.getElementById("submission_doi").value == "") {
        //     document.getElementById("submission_doi").classList.add("text-area-empty");
        //     addError();
        //     valid = false;
        //     break;
        //   }

        //   if (document.getElementById("submission_doi").value != "") {
        //     document.getElementById("submission_doi").classList.remove("text-area-empty");
        //   }
        // }

        if (currentTab == 5 && myInput[i].value == t7_exclude) { // updated currentTab from '7' to '6' for removal of cost calculator, then from '6' to '5' for removal of DOI
          if (document.getElementById("submission_license").value == "") {
            addError();
            valid = false;
            break;
          }

          if (document.getElementById("submission_license").value != "") {
            document.getElementById("submission_license").classList.remove("text-area-empty");
          }
        }

        valid = true;
        removeError();
        document.getElementById("t" + currentTab).focus();

        break;

      } else if (myInput[i].checked == false) {
        valid = false;
        addError();
        window.location.hash = "#form_alert" + currentTab;
      }
    }
  }

  // Loop through text areas
  for (j = 0; j < myTextArea.length; j++) {
    if (myTextArea[j].required) {
      if (myTextArea[j].value == "") {
        valid = false;
        myTextArea[j].classList.add("text-area-empty");
        addError();
      }

      if (myTextArea[j].value != "") {
        myTextArea[j].classList.remove("text-area-empty");
      }
    }
  }

  return valid;
}


// +++ clean up error display +++ //

  // undo disabled nextBtn for radio buttons
  $(function() {

    $('input[type="radio"]').bind('change', function () {
      document.getElementById("nextBtn").classList.remove("disabled");
    });

  });

  // undo disabled nextBtn for text areas (tab 5)
  $( "#t4 textarea" ).on("change", function() { // changed #t5 to #t4 for removal of cost calculator
      
    var zeroCount = 0;

    $(".text-area-empty").each(function() {
      if ( $(this).val().length == 0) {
        zeroCount++
      } else {
        $(this).removeClass( "text-area-empty" );
      }
    });

    if (zeroCount == 0) {
      removeError();
    }
  
  });

  // undo disabled nextBtn for text input (tab 6)
  $(function() {

    $('#t5 input[type="text"]').bind('change', function () { // changed #t6 to #t5 for removal of cost calculator
      if ( $(this).val().length != 0) {
        $(this).removeClass( "text-area-empty" );
        document.getElementById("nextBtn").classList.remove("disabled");
      }
    });

  });

  // undo disabled nextBtn for select (tab 7)
  $( "#t6 select" ).on("change", function() { // changed #t7 to #t6 for removal of cost calculator

    if ($(this).val() != '') {
      document.getElementById("nextBtn").classList.remove("disabled");
      $(this).removeClass( "text-area-empty" );
    } else {
      document.getElementById("nextBtn").classList.add("disabled");
    }

  });


// +++ functions +++ //

// Show/hide text fields
function showMy(theField) {
  document.getElementById(theField + "_wrapper").classList.remove("d-none");
  document.getElementById("submission_" + theField).disabled = false;
}
function hideMy(theField) {
  document.getElementById(theField + "_wrapper").classList.add("d-none");
  document.getElementById("submission_" + theField).disabled = true;
}


// show/hide error wrappers
function addError() {
  document.getElementById("t" + currentTab).classList.add("form-tab-error");
  document.getElementById("form_alert" + currentTab).classList.remove("d-none");
  document.getElementById("nextBtn").classList.add("disabled");
}
function removeError() {
  if (document.getElementById("t" + currentTab).classList.contains("form-tab-error")) {
    document.getElementById("t" + currentTab).classList.remove("form-tab-error");
  }
  if (document.getElementById("form_alert" + currentTab).classList.contains("d-none")) {
    // console.log("no hidden div");
  } else {
    document.getElementById("form_alert" + currentTab).classList.add("d-none");
  }
  if (document.getElementById("nextBtn").classList.contains("disabled")) {
    document.getElementById("nextBtn").classList.remove("disabled");
  }
}


// reset radio button values
function resetRadioButtons() {
  $("#submission_screening_pii_no").prop('checked', false);
    $("#submission_screening_pii_no").parent().removeClass("active");
  $("#submission_screening_pii_yes").prop('checked', false);
    $("#submission_screening_pii_yes").parent().removeClass("active");
  $("#data-size-1").prop('checked', false);
    $("#data-size-1").parent().removeClass("active");
  $("#data-size-2").prop('checked', false);
    $("#data-size-2").parent().removeClass("active");
  $("#data-size-3").prop('checked', false);
    $("#data-size-3").parent().removeClass("active");
  // commented out for removal of cost calculator (RDR-649)
  // $("#submission_cost_calculator_data_size").val("");
  // $("#submission_cost_calculator").val("");
  // $("#submission_cost_calculator_data_size_output").val("");
  // $("#cost-calculator-agreement-1").prop('checked', false);
  // $("#cost-calculator-agreement-1").parent().removeClass("active");
}

// reset agreement radio button
function resetAgreement() {
  $("#deposit-agreement-1").prop( "checked", false );
  $("#deposit-agreement-1").parent().removeClass("active");
}

// reset Globus values
function resetGlobus() {
  $("#submission_using_globus").val('false');
  $("#submission_using_large_files").val('false');
}

// reset text field values
function resetTextFields() {
  $("#submission_title").val("");
  $("#submission_creator").val("");
  $("#submission_contributor").val("");
  $("#submission_affiliation").val("");
  $("#submission_contact").val("");
  $("#submission_description").val("");
  $("#submission_subject").val("");
  $("#submission_based_near").val("");
  $("#submission_temporal").val("");
  $("#submission_language").val("");
  $("#submission_format").val("");
  $("#submission_related_url").val("");
  $("#submission_funding_agency").val("");
  $("#submission_grant_number").val("");
  // $("#submission_doi").val("");
}

// reset calculator values
function resetCostCalculator() {
  $("#submission_cost_calculator_data_size").val("");
  $("#submission_cost_calculator").val("");
  $("#submission_cost_calculator_data_size_output").val("");
  $("#cost-calculator-agreement-1").prop('checked', false);
    $("#cost-calculator-agreement-1").parent().removeClass("active");
}

function clearAllContent() {
  theFormTab = document.getElementsByClassName("form-tab");
  $(theFormTab[0]).hide();
  $(theFormTab[1]).hide();
  $(theFormTab[2]).hide();
  $(theFormTab[3]).hide();
  $(theFormTab[4]).hide();
  $(theFormTab[5]).hide();
  $(theFormTab[6]).hide();
  // $(theFormTab[7]).hide();
}

function updateProgress(n) {

  // console.log('progress: ' + n);
  // console.log("skipSize: " + skipSize);

  // if (skipSize == true) {
    // $("#progress-steps li:nth-child(3)").addClass("d-none"); // hide deposit size step
  // } else {
    // $("#progress-steps li:nth-child(3)").removeClass("d-none"); // show deposit size step
  // }

  // set current tab to current
  $("#progress-steps li:nth-child(" + n + ")").addClass("current").removeClass("complete").attr("aria-current","true");

  // make sure everything after tab n is not current or complete
  i = n + 1;
  while (i <= totalTabs + 1) {
    $("#progress-steps li:nth-child(" + i + ")").removeClass("current").removeClass("complete").attr("aria-current","false");
    $("#progress-steps li:nth-child(" + i + ") span.segment-label span").remove();
    $("#progress-steps li:nth-child(" + i + ") span.segment-label").append('<span class="sr-only">, not completed</span>');
    i++;
  }

  // make sure everything before tab n is complete and not current
  i = 1;
  while (i < n) {
    $("#progress-steps li:nth-child(" + i + ")").addClass("complete").removeClass("current").attr("aria-current","false");
    $("#progress-steps li:nth-child(" + i + ") span.segment-label span").remove();
    $("#progress-steps li:nth-child(" + i + ") span.segment-label").append('<span class="sr-only">, completed</span>');
    i++;
  }

}
