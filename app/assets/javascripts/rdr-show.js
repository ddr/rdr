$(document).ready(function() {

  /* Activate Bootstrap tooltips */
  $('[data-toggle="tooltip"]').tooltip();

  /* Toggle expand/collapse long text, e.g., description field on show page  */
  /* Used in concert with formatted_attribute_renderer.rb which creates the */
  /* accompanying markup. */

  $('a.toggle-extended-text').click(function(e){
    e.preventDefault();
    $(this).toggleClass('expanded');
    $(this).siblings(".expandable-extended-text").toggle();
    if ($(this).text() == '... [Read More]') {
      $(this).text('[Collapse]');
    } else {
      $(this).text('... [Read More]');
    }
  });

  /* Listen for ZIP download / export status when clicking the */
  /* Export Files button or any format in the export dropdown. */
  $('#bulk_export_begin, .bulk_export_dropdown_format').click(function(e) {
    $('#bulk_export_begin').button('loading');
    $('#bulk_export_options_dropdown').addClass('disabled');
    var dataset_id = $('#bulk_export_begin').data('label');
    var cookie_name = 'duke_rdr_export_begin_' + dataset_id;

    // Check every 250ms for the session cookie that indicates the download has begun
    // If found, stop checking & reset the button to default state
    var export_listener = window.setInterval(function () {
      if (document.cookie.indexOf(cookie_name) !== -1) {
          // Wait 2 sec, then reset the buttons & clear the cookie
          // by setting a past expiration
          setTimeout(function(){
            $('#bulk_export_begin').button('reset');
            $('#bulk_export_options_dropdown').removeClass('disabled');
            document.cookie = cookie_name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
          }, 2000);
          clearInterval(export_listener);
      }
    }, 250);
  });

  $('a.vertical-breadcrumb-expander').click(function(e){
    e.preventDefault();
    $(this).toggleClass('expanded');
    $('.vertical-breadcrumb .can-collapse').slideToggle();
    if ($(this).text() == '[Show More...]') {
      $(this).text('[Collapse]');
    } else {
      $(this).text('[Show More...]');
    }
  });

});
