$(document).ready(() => {
  $('#readme-tab').click(() => {
    fetchReadme($('#readme-preview-link').data('preview-url'));
  });
  $('#view-text').click(() => {
    fetchReadme($('#readme-preview-link').data('preview-url'), 'text');
  });
  $('#view-markdown').click(() => {
    fetchReadme($('#readme-preview-link').data('preview-url'), 'markdown');
  });
  // If page is loaded with the #readme tab active, fetch the README 
  if ($('#readme-tab').hasClass('active')) {
    fetchReadme($('#readme-preview-link').data('preview-url'));
  }
});

const renderReadme = (text, overrideDefault) => {
  const reader = new commonmark.Parser();
  const writer = new commonmark.HtmlRenderer();
  const isMarkdown = $('#readme-preview-link').data('is-markdown') === true;
  try {
    const readmePaneRaw = $('#readme-raw');
    const readmePaneMarkdown = $('#readme-markdown');
    const showRawBtn = $('#view-text');
    const showMarkdownBtn = $('#view-markdown');
    if ((!isMarkdown && overrideDefault !== 'markdown') || overrideDefault === 'text') {
      readmePaneMarkdown.hide();
      showRawBtn.hide();
      showMarkdownBtn.show();
      readmePaneRaw.show();
      readmePaneRaw.text(text);
    } else {
      const parsed = reader.parse(text);
      const result = writer.render(parsed);
      readmePaneRaw.hide();
      showMarkdownBtn.hide();
      showRawBtn.show();
      readmePaneMarkdown.show();
      readmePaneMarkdown.html(result);
    }
    toggleLoadingSpinner();
  } catch (e) {
    console.log(e);
    toggleLoadingSpinner();
  }
}

const toggleLoadingSpinner = (on = false) => {
  setTimeout(() => {
    $('#readme-pane').find('.spinner-border').toggle(on);
  }, 500);
}

const preventReload = () => {
  $('#readme-tab').off();
}

const showErrorMessage = () => {
  toggleLoadingSpinner();
  $('#readme-raw').text('Our apologies. We were unable to retrieve the readme file.');
  // Pretty unnecessary, but just to show the error message with a slight delay
  setTimeout(() => {
    $('#readme-raw').show();
  }, 600);
}

const fetchReadme = (fetchUrl, overrideDefault = '') => {
  fetch(fetchUrl)
    .then((response) => {
      if (!response.ok) {
        showErrorMessage();
      } else {
        response.text().then((text) => {
          renderReadme(text, overrideDefault);
        });
      }
    })
    .catch(() => {
      showErrorMessage();
  });
  preventReload();
}