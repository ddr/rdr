# frozen_string_literal: true

module Hyrax
  class RdrFileSetPresenter < Hyrax::FileSetPresenter

    # modify this attribute to use an alternate presenter class for the collections
    collection_presenter_class = Hyrax::CollectionPresenter
    presenter_factory_class = Hyrax::MemberPresenterFactory

    delegate :top_level, :digest, to: :solr_document

    include Hyrax::CollectionPresentable
    
    # Overrides for CharacterizationBehavior fields
    def self.characterization_terms
      [
        :byte_order, :compression, :height, :width, :color_space,
        :profile_name, :profile_version, :orientation, :color_map, :image_producer,
        :capture_device, :scanning_software, :gps_timestamp, :latitude, :longitude,
        :file_format, :file_title, :page_count, :duration, :sample_rate,
        :format_label, :file_size, :filename, :well_formed, :last_modified,
        :original_checksum, :mime_type
      ]
    end

    # Hacky but cleanest way I could think to shim the sha1hash into original_checksum
    def original_checksum
      digest&.map { |d| d&.sub(/^urn:sha1:/, '') }
    end
     
    # Override the method from Hyrax::CollectionPresentable b/c FileSet doesn't have ancestor trail
    def ancestor_trail
      ancestor_trail_list = ancestor_trail_ids(parent&.solr_document || solr_document)
      parent_id = request.params[:parent_id]
      # If parent is not nil, then add to end of ancestor_trail_list
      ancestor_trail_list.unshift(parent_id) if parent_id
      docs = ancestor_trail_list.map { |id| ::SolrDocument.find(id) }
      docs.reverse
    end

    def readme?
      filename = title&.first&.downcase
      mime_type == 'text/plain' && filename.include?('readme') && file_format.include?('Plain text');
    end

    def markdown_readme?
      filename = title&.first.downcase
      readme? && (filename.end_with?('.md') || file_format.include?('Markdown'))
    end

    def media_download_link
      Hyrax::Engine.routes.url_helpers.download_path(id, host: request.host)
    end
  end
end