# frozen_string_literal: true

# Generated via
#  `rails generate hyrax:work Dataset`
module Hyrax
  class DatasetPresenter < Hyrax::WorkShowPresenter
    # Account for fields not already delegated to solr_document via
    # https://github.com/samvera/hyrax/blob/main/app/presenters/hyrax/work_show_presenter.rb
    delegate  :affiliation, :ark, :available, :based_near,
              :bibliographic_citation, :contact, :doi, :format,
              :funding_agency, :grant_number, :title, :creator,
              :in_works_ids, :is_replaced_by, :members, :provenance,
              :related_url, :replaces, :resource_type, :temporal, :top_level, to: :solr_document

    delegate(*Rdr::DatasetVersioning.public_instance_methods, to: :solr_document)

    delegate :file_set_presenters, to: :member_presenter_factory

    include Hyrax::CollectionPresentable

    class_attribute  :presenter_factory_class, :work_presenter_class

    self.presenter_factory_class = Hyrax::RdrMemberPresenterFactory
    self.work_presenter_class = DatasetPresenter

    def depositor?
      current_ability.current_user.user_key == depositor
    end

    def assignable_doi?
      current_ability.can?(:assign_register_doi, solr_document) &&
        ark.present? &&
        doi.blank? &&
        title.present? && creator.present? && available.present?
    end

    def file_scan
      @file_scan ||= WorkFilesScanner.call(id, current_ability)
    end

    delegate :file_count, to: :file_scan

    delegate :file_size_total, to: :file_scan

    def small_dataset?
      file_size_total.bytes < Rdr.globus_only_gb_threshold.gigabytes
    end

    def deaccessioned?
      (file_count == 0) && provenance.present? && doi.present?
    end

    # Export (ZIP) & Globus buttons & labeling
    # Note that both features rely on files being exported to the
    # Globus-mounted storage

    def globus_url
      globus_export&.url_for || '#'
    end

    def export_btn_classes
      classes = %w[btn btn-success btn-download]
      classes << 'disabled' unless globus_ready?
      classes
    end

    def globus_download_btn_classes
      classes = %w[btn btn-secondary btn-download]
      classes << 'disabled' unless globus_ready?
      classes
    end

    def export_btn_title
      if globus_ready?
        I18n.t('hyrax.base.show_actions.export_files')
      else
        I18n.t('hyrax.base.show_actions.export_files_soon').html_safe
      end
    end

    def globus_download_btn_title
      if globus_ready?
        I18n.t('hyrax.base.show_actions.globus_download')
      else
        I18n.t('hyrax.base.show_actions.globus_download_soon').html_safe
      end
    end

    def globus_download_btn_tooltip
      if globus_processing?
        I18n.t('hyrax.base.show_actions.globus_tooltip_soon')
      elsif globus_ready? && !small_dataset?
        I18n.t('hyrax.base.show_actions.globus_tooltip_large',
               filesize: ActiveSupport::NumberHelper.number_to_human_size(file_size_total))
      else
        I18n.t('hyrax.base.show_actions.globus_tooltip')
      end
    end

    def export_btn_tooltip
      globus_processing? ? I18n.t('hyrax.base.show_actions.export_files_tooltip_soon') : ''
    end

    def globus_processing?
      globus_export&.new? || globus_export&.built?
    end

    def globus_ready?
      globus_export&.exported?
    end

    # Top-level datasets find their own associated Globus export.
    # Nested datasets find their top-most parent work's Globus export.
    def globus_export
      @globus_export ||= Globus::Export.find_by(dataset_id: toplevel_work_id)
    end

    # Overrides 'Hyrax::WorkShowPresenter#grouped_presenters' to add in the presenters for works in which the current
    # work is nested
    def grouped_presenters(filtered_by: nil, except: nil)
      super.merge(grouped_work_presenters(filtered_by: filtered_by, except: except))
    end

    # modeled on '#grouped_presenters' in Hyrax::WorkShowPresenter, which returns presenters for the collections of
    # which the work is a member
    def grouped_work_presenters(filtered_by: nil, except: nil)
      grouped = in_work_presenters.group_by(&:model_name).transform_keys { |key| key.to_s.underscore }
      grouped.select! { |obj| obj.downcase == filtered_by } unless filtered_by.nil?
      grouped.except!(*except) unless except.nil?
      grouped || {}
    end

    # modeled on '#member_of_collection_presenters' in Hyrax::WorkShowPresenter
    def in_work_presenters
      PresenterFactory.build_for(ids: in_works_ids,
                                 presenter_class: work_presenter_class,
                                 presenter_args: presenter_factory_arguments)
    end

    # Per RDR-654, override a method from Hyrax::WorkShowPresenter to patch a bug
    # where an "undefined method `.image?`" error is thrown for:
    # - a public dataset
    # - with nested private descendant datasets
    # - which contain PDF or image files

    # I.e., the iiif_viewer? method calls representative_presenter.image? -- the
    # representative_presenter is assumed to return a FileSetPresenter yet in the
    # scenario above it is returning a WorkShowPresenter which does not respond to .image?

    # ActionView::Template::Error (undefined method `image?' for #<Hyrax::WorkShowPresenter:0x00007fad948c1088>)
    # https://github.com/samvera/hyrax/blob/v3.2.0/app/presenters/hyrax/work_show_presenter.rb#L92-L101
    # https://github.com/samvera/hyrax/blob/v3.2.0/app/presenters/hyrax/work_show_presenter.rb#L63-L70

    # @return FileSetPresenter presenter for the representative FileSets
    def representative_presenter
      return nil if representative_id.blank?
      return nil if file_set_presenters.blank? # <-- DUL CUSTOM BUGFIX
      @representative_presenter ||=
        begin
          result = member_presenters([representative_id]).first
          return nil if result.try(:id) == id
          result.try(:representative_presenter) || result
        rescue Hyrax::ObjectNotFoundError
          Hyrax.logger.warn "Unable to find representative_id #{representative_id} for work #{id}"
          return nil
        end
    end

    def readme_file_set
      @readme_file_set ||= file_set_presenters.find { |file_set| file_set.readme? }
    end

    private

    def member_presenter_factory
      @member_presenter_factory ||=
        if valkyrie_presenter?
          PcdmMemberPresenterFactory.new(solr_document, current_ability)
        else
          self.class
              .presenter_factory_class
              .new(solr_document, current_ability, request)
        end
    end

    # Modeled on '#member_of_collection_presenters' in Hyrax::WorkShowPresenter
    # Get the presenters for all the collections in which the top-level work is a
    # member, no matter what collection type or how deeply nested the current work is.
    # Limit to collections the current user can access.
    #
    # @return [Array<CollectionPresenter>] presenters
    def toplevel_work_collection_presenters
      return [] if toplevel_work_id.blank?

      PresenterFactory.build_for(ids: toplevel_authorized_collections,
                                 presenter_class: collection_presenter_class,
                                 presenter_args: presenter_factory_arguments)
    end
  end
end
