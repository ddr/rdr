# frozen_string_literal: true

module Hyrax
  class RdrMemberPresenterFactory < Hyrax::MemberPresenterFactory
    class_attribute :file_presenter_class
    self.file_presenter_class = Hyrax::RdrFileSetPresenter

    # call parent constructor
    def initialize(solr_document, current_ability, request = nil)
      super(solr_document, current_ability, request)
    end
  end
end