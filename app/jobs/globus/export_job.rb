# frozen_string_literal: true

module Globus
  class ExportJob < ::ApplicationJob
    def perform(export)
      export.run
    end
  end
end
