# frozen_string_literal: true

module Globus
  class RefreshMetadataJob < ::ApplicationJob
    def perform(force: false)
      Export.refresh_metadata(force: force)
    end
  end
end
