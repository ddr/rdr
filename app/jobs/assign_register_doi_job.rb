# frozen_string_literal: true

class AssignRegisterDoiJob < ApplicationJob
  queue_as :doi

  def perform(work)
    AssignDoi.call(work)

    work.reload

    if work.doi_registerable?
      DataciteRegistration.call(work)
    else
      logger.debug { "The DOI for Dataset #{work.id} is not registerable." }
    end
  end
end
