# frozen_string_literal: true

require 'importer/batch_object_importer'

class BatchObjectImportJob < ApplicationJob
  queue_as Hyrax.config.ingest_queue_name
  sidekiq_options retry: false

  def perform(model, attributes, files_directory)
    Importer::BatchObjectImporter.call(model, attributes, files_directory)
  end
end
