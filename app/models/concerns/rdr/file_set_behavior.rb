module Rdr
  module FileSetBehavior
    extend ActiveSupport::Concern
    include Hyrax::FileSetBehavior
    include Rdr::FileSetDerivatives
  end
end
