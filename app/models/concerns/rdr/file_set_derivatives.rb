module Rdr
  module FileSetDerivatives
    extend ActiveSupport::Concern
    include Hyrax::FileSet::Derivatives

    private

      def file_set_derivatives_service
        Rdr::RdrDerivativeService.for(self)
      end
  end
end
