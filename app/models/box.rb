# frozen_string_literal: true

module Box
  def self.table_name_prefix
    'box_'
  end
end
