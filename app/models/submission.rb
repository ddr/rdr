# frozen_string_literal: true

class Submission
  include ActiveModel::Model

  SCREENING_ATTRIBUTES = %i[screening_pii screening_pace screening_data_size cost_calculator_fund_code
                            cost_calculator_data_size cost_calculator cost_calculator_data_size_output
                            cost_calculator_agreement deposit_agreement].freeze

  SUBMISSION_ATTRIBUTES = %i[title creator contributor affiliation contact description subject
                             based_near temporal language format related_url funding_agency
                             grant_number doi_exists doi using_cc0 license using_globus using_pace
                             using_large_files user_agent].freeze

  FOLLOWUP_ATTRIBUTES = %i[more_information followup display_name netid].freeze

  ATTRIBUTES = SCREENING_ATTRIBUTES + SUBMISSION_ATTRIBUTES + FOLLOWUP_ATTRIBUTES

  # Readiness to submit data
  READY = 'ready'
  NOT_READY = 'not ready'

  # Data size
  SMALL_DATA_SIZE = 'Less than 10GB'
  MEDIUM_DATA_SIZE = 'Between 10GB and 300GB'
  LARGE_DATA_SIZE = 'Greater than 300GB'

  # storage calculator multipliers
  STORAGE_MULTIPLIER_LEVEL1 = ENV.fetch('STORAGE_MULTIPLIER_LEVEL1', '0.282').to_f
  STORAGE_MULTIPLIER_LEVEL2 = ENV.fetch('STORAGE_MULTIPLIER_LEVEL2', '1.41').to_f
  STORAGE_MULTIPLIER_LEVEL3 = ENV.fetch('STORAGE_MULTIPLIER_LEVEL3', '1.97').to_f
  STORAGE_MULTIPLIER_LEVEL4 = ENV.fetch('STORAGE_MULTIPLIER_LEVEL4', '2.82').to_f
  STORAGE_MULTIPLIER_LEVEL5 = ENV.fetch('STORAGE_MULTIPLIER_LEVEL5', '7.05').to_f

  # Agreement to data storage cost
  AGREE_COST = 'I agree'
  NOT_AGREE_COST = 'I do not agree'

  # Agreement to deposit agreement
  AGREE = 'I agree'
  NOT_AGREE = 'I do not agree' # we're forcing to agree or nothing - do we need this still?

  # Willingness to use cc0
  USE_CC0 = 'will use cc0'
  NOT_USE_CC0 = 'will not use cc0'

  attr_accessor :submitter, *ATTRIBUTES

  with_options if: :passed_screening? do
    validates :title, :creator, :description, :subject, :doi_exists, :using_cc0, presence: true
    validates :doi, presence: { if: :existing_doi? }
    validates :license, presence: { unless: :use_cc0? }
  end

  def passed_screening?
    deposit_agreement == AGREE
  end

  def followup?
    followup == 'true'
  end

  def using_globus?
    using_globus == 'true'
  end

  def using_pace?
    using_pace == 'true'
  end

  def using_large_files?
    using_large_files == 'true'
  end

  private

  def existing_doi?
    doi_exists == 'yes'
  end

  def use_cc0?
    using_cc0 == USE_CC0
  end
end
