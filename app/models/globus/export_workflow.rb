# frozen_string_literal: true

module Globus
  module ExportWorkflow
    extend ActiveSupport::Concern

    included do
      #
      # Workflow
      #
      # The implicitly defined '*!' methods trigger workflow transitions
      # and the matching explicit methods w/o '!' execute workflow actions.
      #
      # Current state of export is persisted in 'workflow_state' column.
      #
      # workflow gem: https://github.com/geekq/workflow
      #
      include WorkflowActiverecord

      workflow do
        state :new do
          event :build, transitions_to: :built
        end

        state :built do
          event :reset, transitions_to: :new
          event :export, transitions_to: :exported
        end

        state :exported do
          event :reset, transitions_to: :new
        end

        on_transition do |from, to, _triggering_event, *_event_args|
          log(format("Transitioned from '%s' to '%s'", from, to))
        end
      end
    end
  end
end
