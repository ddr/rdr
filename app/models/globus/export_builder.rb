# frozen_string_literal: true

require 'English'
require 'fileutils'
require 'digest'

module Globus
  #
  # Globus::Export helper class for building the export.
  #
  # @api private
  class ExportBuilder
    attr_reader :export

    delegate :log, :export_path, :dataset, :dataset_id,
             to: :export

    def initialize(export)
      @export = export
    end

    def to_s
      'Builder for %s' % export
    end

    def build
      download(dataset, '.')
      add_manifest
      check_manifest
    end

    def get_work(id)
      SolrDocument.find(id)
    end

    def download(work, path)
      FileUtils.mkdir_p(path)

      FileUtils.cd(path) do
        download_contents(work)
      end
    end

    def download_contents(work)
      log('Adding work: %s' % work)

      add_files(work)
      add_nested_works(work)

      log('Work added: %s' % work)
    end

    def add_files(work)
      file_sets(work).each do |file_set|
        file = file_set.original_file
        path = file_set.title&.first&.presence || file&.original_name

        add_file(file, path)
        validate_file(file, path)
        add_to_manifest(file, path)
      end
    end

    def sha1(file, path)
      if file.checksum.algorithm == 'SHA1'
        file.checksum.value
      else
        Digest::SHA1.file(path).hexdigest
      end
    end

    def add_to_manifest(file, path)
      abspath = File.absolute_path(path)
      relpath = export.relative_path(abspath)
      manifest << [sha1(file, path), relpath]
    end

    def validate_file(file, path)
      alg = file.checksum.algorithm
      klass = Digest.const_get(alg)
      digest = klass.file(path).hexdigest

      if digest == file.checksum.value
        log('File download verified: %s' % path)
      else
        raise ExportError,
              "Checksum error on #{file}: " \
              "Expected #{alg} digest #{file.checksum.value}; " \
              "computed #{digest} at path #{path}."
      end
    end

    def add_file(file, path)
      File.open(path, 'wb') do |f|
        file.stream.each { |chunk| f.write(chunk) }
      end

      log('File added: %s' % path)
    end

    def add_nested_works(work)
      nested_works(work).each do |nested_work|
        path = nested_work.title.first.strip
        download(nested_work, path)
      end
    end

    def manifest
      @manifest ||= []
    end

    def add_manifest
      File.open(Rdr.globus_export_manifest_filename, 'w') do |f|
        manifest.each do |entry|
          f.puts entry.join('  ')
        end
      end

      log('SHA1 manifest written.')
    end

    def check_manifest
      cmd = 'sha1sum -c --strict %s' % Rdr.globus_export_manifest_filename

      log IO.popen(cmd, err: %i[child out], &:read)

      if $CHILD_STATUS.success?
        log('SHA1 manifest check succeeded.')
      else
        raise ExportError, 'SHA1 manifest check failed.'
      end
    end

    # Logic adapted from Hyrax::MemberPresenterFactory#ordered_ids
    def member_ids(work)
      query = "proxy_in_ssi:#{work.id}"
      results = ActiveFedora::SolrService.query(query, rows: 10_000, fl: 'ordered_targets_ssim')
      results.flat_map { |x| x.fetch('ordered_targets_ssim', []) }
    end

    # Logic adapted from Hyrax::MemberPresenterFactory#file_set_ids
    def file_set_ids(work)
      query = '{!field f=has_model_ssim}FileSet'
      filter = "{!join from=ordered_targets_ssim to=id}id:\"#{work.id}/list_source\""
      results = ActiveFedora::SolrService.query(query, rows: 10_000, fl: ActiveFedora.id_field, fq: filter)
      results.flat_map { |x| x.fetch(ActiveFedora.id_field, []) }
    end

    # Logic suggested by Hyrax::MemberPresenterFactory#work_presenters
    def nested_work_ids(work)
      member_ids(work) - file_set_ids(work)
    end

    def nested_works(work)
      nested_work_ids(work).map { |id| get_work(id) }.select(&:public?)
    end

    def file_sets(work)
      file_set_ids(work).map { |id| FileSet.find(id) }.select(&:public?)
    end
  end
end
