# frozen_string_literal: true

require 'fileutils'
require 'find'
require 'pathname'

module Globus
  # Exception class for export-specific errors.
  class ExportError < Rdr::Error; end

  #
  # Encapsulates a Globus export request.
  #
  # The public API consists principally of the class methods.
  # For the most part, you should be able to use
  # `Globus::Export.call(dataset_id)'; if you want to instantiate
  # the export but neither run nor enqueue it, you may use
  # `new' and `run'.
  #
  class Export < ::ApplicationRecord
    # Whether to execute exports asynchronously by default
    ASYNC_DEFAULT = false

    # File system permissions applied during build
    BUILD_MODE = 0o700

    # File system permissions applied to export recursively
    EXPORT_MODE = '-wx+rX'

    self.table_name = 'globus_exports'

    # Only one export per dataset
    validates :dataset_id, uniqueness: true

    # Only public (visibility: open) and top-level datasets are exported
    validate :dataset_must_be_public,
             :dataset_must_be_top_level,
             on: :create

    # Remove exported files after destroying database record
    after_destroy :wipe!

    delegate :title, to: :metadata

    include ExportWorkflow

    # @param dataset_id [String] Dataset ID
    # @return [String] Globus URL to dataset
    def self.url_for(dataset_id)
      # https://app.globus.org/file-manager?origin_id=b69b1552-13c8-11eb-81b3-0e2f230cc907&origin_path=%2F7s75dd04w%2F
      u = URI(Rdr.globus_base_url)
      u.query = URI.encode_www_form(origin_id: Rdr.globus_export_origin_id, origin_path: "/#{dataset_id}/")
      u.to_s
    end

    # Main public API entrypoint
    #
    # @param dataset_or_id [Dataset, SolrDocument, String] the Dataset to expor
    # @param async [Boolean] whether to run asynchronously (default: false)t
    # @return [Globus::Export] the export
    def self.call(dataset_or_id, async: ASYNC_DEFAULT)
      dataset_id = dataset_or_id.try(:id) || dataset_or_id

      wfs = WorkFilesScanner.call(dataset_id, Ability.new(nil))
      if wfs.file_count.zero?
        logger.warn { "Dataset ID #{dataset_id} has no public files and will not be exported." }
      else
        find_or_create_by!(dataset_id: dataset_id).tap do |export|
          export.run(async: async)
        end
      end
    end

    def self.stale
      Enumerator.new do |yielder|
        find_each do |export|
          yielder << export if export.stale?
        end
      end
    end

    def self.refresh_metadata(force: false)
      if force
        find_each(&:refresh_metadata)
      else
        stale.each(&:refresh_metadata)
      end
    end

    def to_s
      "Globus Export: Dataset ID #{dataset_id}"
    end

    def refresh_metadata
      FileUtils.cd(export_path) do
        FileUtils.chmod('u+w', Rdr.globus_export_readme_filename)
        add_readme
        touch
        FileUtils.chmod('u-w', Rdr.globus_export_readme_filename)
      end
    end

    def url_for
      self.class.url_for(dataset_id)
    end

    # Metadata fields in Globus::ExportMetadata assume the SolrDocument representation.
    # @return [SolrDocument] the SolrDocument representation of the dataset
    def dataset
      @dataset ||= get_dataset
    end

    #
    # Main workflow entry point
    #

    def run(async: ASYNC_DEFAULT)
      return unless can_build? || can_export?
      return run_async if async

      build! if can_build?
      export! if can_export?
    end

    # Push to a job processing queue
    def run_async
      ExportJob.perform_later(self)
    end

    #
    # Workflow callbacks
    #

    def build
      FileUtils.mkdir_p(export_path, mode: BUILD_MODE)

      FileUtils.cd(export_path) do
        builder.build
        add_readme
      end
    end

    def export
      FileUtils.chmod_R(EXPORT_MODE, export_path)
    end

    def reset
      wipe!
    end

    #
    # Other methods
    #

    # Filesystem path to export during build process
    def export_path
      File.join(Rdr.globus_export_directory, dataset_id)
    end

    def builder
      @builder ||= ExportBuilder.new(self)
    end

    def metadata
      @metadata ||= ExportMetadata.new(self)
    end

    def wipe!
      if File.directory?(export_path)
        FileUtils.chmod_R(BUILD_MODE, export_path)
        FileUtils.rm_r(export_path)
      end
    end

    def log(msg)
      logger.info format('%s - %s', self, msg)
    end

    def file_paths
      Find.find(export_path).select { |path| File.file?(path) }
    end

    def relative_path(path)
      pathname = Pathname.new(path)

      if pathname.absolute?
        pathname.relative_path_from(export_path).to_s
      else
        # We hope the path is relative to export_path ...
        path
      end
    end

    # We need this for the zip streamer
    def relative_file_paths
      file_paths.map { |path| relative_path(path) }
    end

    def stale?
      exported? && updated_at < dataset_updated
    end

    def readme_content
      ActionController::Base.renderer.render(
        template: 'globus/export_readme',
        locals: { metadata: metadata }
      )
    end

    def add_readme
      File.write(Rdr.globus_export_readme_filename, readme_content)

      log('README file added.')
    end

    def get_dataset
      SolrDocument.find(dataset_id)
    end

    def dataset_updated
      # NOTE: Hyrax::SolrDocument::Metadata coerces Solr date/time into Date (!)
      DateTime.parse get_dataset['system_modified_dtsi']
    end

    #
    # Validation
    #

    def dataset_must_be_public
      errors.add(:dataset_id, 'is not public') unless dataset.public?
    end

    def dataset_must_be_top_level
      errors.add(:dataset_id, 'is not top_level') unless dataset.top_level
    end
  end
end
