# frozen_string_literal: true

module Globus
  #
  # Helper class for handling export metadata during the build process
  #
  class ExportMetadata
    FIELDS = %w[ abstract
                 access_right
                 affiliation
                 alternative_title
                 ark
                 based_near_label
                 bibliographic_citation
                 contact
                 contributor
                 creator
                 date_created
                 description
                 doi
                 format
                 funding_agency
                 grant_number
                 is_replaced_by
                 keyword
                 language
                 license
                 provenance
                 publisher
                 related_url
                 replaces
                 resource_type
                 rights_notes
                 rights_statement
                 subject
                 temporal
                 title ].freeze

    attr_reader :export

    delegate :file_size_total, :file_count, to: :files

    def initialize(export)
      @export = export
    end

    def title
      values(:title).first
    end

    def fields
      FIELDS.select { |field| raw(field).present? }
    end

    # @return String formatted value
    # @param [String, Symbol] field
    def value(field)
      values(field).join('; ')
    end

    # @return [Array<String>] value coerced to an aaray
    # @param [String, Symbol] field
    def values(field)
      Array(raw(field))
    end

    # @return [String, Array<String>] raw value
    # @param [String, Symbol] field
    def raw(field)
      export.dataset.send(field)
    end

    def pubdate
      values(:available).map do |val|
        DateTime.parse(val).strftime('%Y-%m-%d')
      end.join('; ')
    end

    def exported_at
      export.updated_at
    end

    def files
      @files ||= WorkFilesScanner.call(export.dataset_id)
    end
  end
end
