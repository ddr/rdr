# frozen_string_literal: true

module Globus
  class ExportAll
    def self.query
      ActiveFedora::SolrService.query('has_model_ssim:Dataset',
                                      fq: ['top_level_bsi:true', 'visibility_ssi:open'],
                                      rows: 100_000)
    end

    def self.call
      query.each { |dataset| Export.call(dataset, async: true) }
    end
  end
end
