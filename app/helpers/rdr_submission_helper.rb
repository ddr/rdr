# frozen_string_literal: true

module RdrSubmissionHelper
  def include_submission_entry?(submission, field)
    submission.send(field).present? if %w[using_globus screening_data_size].include?(field.to_s) == false
  end

  def submission_entry(submission, field)
    label = t("rdr.submissions.email.label.#{field}")
    "#{label}: #{submission.send(field)}"
  end

  def submission_folder_url(folder_id)
    "#{Rdr.box_base_url_rdr_submissions}/folder/#{folder_id}"
  end
end
