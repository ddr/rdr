# frozen_string_literal: true

class AssignDoi
  DOI_PREFIX = Rdr.doi_prefix

  attr_accessor :work

  def self.call(work)
    new(work).assign!
  end

  def initialize(work)
    @work = work
  end

  def assign!
    if work.doi_assignable?
      ark_suffix = work.ark.split('/').last
      doi = [DOI_PREFIX, ark_suffix].join('/')
      work.doi = doi
      raise Rdr::DoiAssignmentError, I18n.t('rdr.doi.assignment_failed') unless work.save
    else
      raise Rdr::DoiAssignmentError, I18n.t('rdr.doi.not_assignable')
    end
  end

  def self.transform_doi_prefixes!
    ActiveFedora::Base.find_each(has_model_ssim: 'Dataset') do |dataset_work|
      next unless dataset_work.is_a?(Dataset) &&
                  dataset_work.doi.present?

      original_doi = dataset_work.doi
      doi = dataset_work.doi.split('/')
      doi[0] = DOI_PREFIX
      dataset_work.doi = doi.join('/')
      $stderr.puts "#{original_doi}: #{I18n.t('rdr.doi.assignment_failed')}" unless dataset_work.save
    end
  end
end
