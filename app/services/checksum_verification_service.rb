# frozen_string_literal: true

class ChecksumVerificationService
  def self.call(*args)
    event = ActiveSupport::Notifications::Event.new(*args)
    wrapper = event.payload[:wrapper]
    ChecksumVerificationJob.perform_later(wrapper)
  end

  attr_reader :wrapper

  def initialize(wrapper)
    @wrapper = wrapper
  end

  def verify_checksum
    fileset = wrapper.file_set
    if fileset.files.empty?
      raise Rdr::ChecksumInvalid, I18n.t('rdr.fileset_without_files', fs_id: fileset.id)
    else
      if provided_checksum = Importer::Checksum.checksum(wrapper.path)
        verify_provided_checksum(fileset, provided_checksum)
      end
      verify_repository_checksum(fileset)
    end
  end

  # Verifies the checksum stored in the repository against a provided checksum.
  def verify_provided_checksum(fileset, provided_checksum)
    repo_checksum = fileset.original_file.checksum.value
    unless repo_checksum == provided_checksum
      raise Rdr::ChecksumInvalid,
            I18n.t('rdr.provided_checksum_mismatch', fs_id: fileset.id, provided_cksum: provided_checksum,
                                                     repo_cksum: repo_checksum)
    end
  end

  # Checks that the checksum of the file on disk
  # matches the checksum stored in the repository.
  def verify_repository_checksum(fileset)
    # Skip Fedora-internal files that don't have a path
    fileset_path = fileset.title&.first&.presence || fileset.original_file&.original_name
    if (fileset_path.present? && File.exist?(fileset_path))
      alg = fileset.original_file.checksum.algorithm
      klass = Digest.const_get(alg)
      disk_checksum = klass.file(fileset_path).hexdigest
      repo_checksum = fileset.original_file.checksum.value
      unless repo_checksum == disk_checksum
        raise Rdr::ChecksumInvalid,
              I18n.t('rdr.repository_checksum_failure', fs_id: fileset.id, file_cksum: disk_checksum,
                                                       repo_cksum: repo_checksum)
      end
    else
      Rails.logger.info("Skipping fixity check for: #{fileset.inspect}")
    end
  end
end
