# frozen_string_literal: true

require 'json'
class DataciteRegistration
  def self.datacite_request_body(work)
    metadata = DataciteMetadata.call(work)
    metadata['event'] = 'publish'
    return {
      'data': {
        'id': work.doi,
        'type': 'dois',
        'attributes': metadata
      }
    }.to_json
  end

  def self.call(work, dryrun: false)
    register_doi(datacite_request_body(work))
  end

  def self.update(work, dryrun: false)
    update_doi(datacite_request_body(work), work.doi)
  end

  def self.list_missing_dois
    unregistered_datasets_with_dois.each do |unregistered_dataset_with_doi|
      puts unregistered_dataset_with_doi.doi
    end
  end

  def self.mint_dois_for_datasets(datasets_to_mint)
    datasets_to_mint.each do |dataset_to_mint|
      begin
        self.call(dataset_to_mint)
      rescue Rdr::DataciteRegistrationError
        puts "Rdr::DataciteRegistrationError minting DOI: #{dataset_to_mint.doi}"
      end
    end
  end

  def self.update_doi_metadata_for_datasets(datasets_to_update)
    datasets_to_update.each do |dataset_to_update|
      begin
        self.update(dataset_to_update)
      rescue Rdr::DataciteRegistrationError
        puts "Rdr::DataciteRegistrationError updating DOI metadata: #{dataset_to_update.doi}"
      end
    end
  end

  def self.mint_missing_dois
    self.mint_dois_for_datasets(unregistered_datasets_with_dois)
  end

  def self.update_all_datasets_with_dois
    self.update_doi_metadata_for_datasets(datasets_with_dois)
  end

  def self.datasets_with_dois
    datasets = []
    ActiveFedora::Base.find_each(has_model_ssim: 'Dataset') do |potential_dataset|
      next unless potential_dataset.is_a?(Dataset) &&
                  potential_dataset.doi.present?

      datasets << potential_dataset
    end
    return datasets
  end

  def self.registered_dois
    registered_dois_list = []
    datacite_filter = "provider-id=#{Rdr.datacite_provider_id}&"
    if Rdr.datacite_user.include?('.')
      datacite_filter = "client-id=#{Rdr.datacite_user.downcase}&"
    end
    uri = URI("https://#{Rdr.datacite_host}/dois?#{datacite_filter}page[cursor]=1")
    loop do
      https = Net::HTTP.new(uri.host, uri.port)
      https.use_ssl = true
      request = Net::HTTP::Get.new(uri.request_uri)
      request['Accept'] = 'application/json'
      response = https.request(request)
      registered_data = JSON.parse(response.body)
      registered_dois_list += registered_data['data'].map{|d| d.dig('attributes', 'doi')&.downcase}
      next_link = registered_data.dig('links','next')
      break unless next_link.present?

      uri = URI(next_link)
    end
    return registered_dois_list
  end

  def self.unregistered_datasets_with_dois
    registered_dois_list = registered_dois()
    unregistered_datasets = []
    
    datasets_with_dois.each do |potential_dataset|
      next if registered_dois_list.include?(potential_dataset.doi.downcase)

      unregistered_datasets << potential_dataset
    end
    return unregistered_datasets
  end

  def self.register_doi(datacite_request_body, http_method: Net::HTTP::Post, api_endpoint: "https://#{Rdr.datacite_host}/dois")
    Rails.logger.info("DataciteRegistration.register_doi for #{http_method} with payload: #{datacite_request_body}")
    uri = URI(api_endpoint)
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    request = http_method.new(uri.request_uri)
    request.basic_auth Rdr.datacite_user, Rdr.datacite_password
    request['Content-Type'] = 'application/vnd.api+json'
    request.body = datacite_request_body
    response = https.request(request)
    Rails.logger.info("DataciteRegistration.register_doi response: #{response.code} #{response.message} #{response.body}")
    raise Rdr::DataciteRegistrationError unless response.code.start_with?('2')
  end

  def self.update_doi(datacite_request_body, doi)
    register_doi(datacite_request_body, http_method: Net::HTTP::Put, api_endpoint: "https://#{Rdr.datacite_host}/dois/#{doi}")
  end
end
