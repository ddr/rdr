# frozen_string_literal: true

class DataciteMetadata
  BASE_RESOURCE_URL = 'https://idn.duke.edu/'

  def self.call(work)
    new(work).call
  end

  attr_reader :work

  def initialize(work)
    @work = work
  end

  def call
    set_doi
    set_creators
    set_titles
    set_publisher
    set_publication_year
    set_types
    set_url
    set_funding_references
    set_rights_list
    set_descriptions
    set_dates
    set_subjects
    set_contributors
    set_alternate_identifiers
    set_related_identifiers
    document
  end

  def document
    @document ||= {'schemaVersion': 'http://datacite.org/schema/kernel-4'}
  end

  def set_titles
    document['titles'] = work.title.map{|title| {'title': title}}
  end

  def set_creators
    document['creators'] = work.creator.map{|creator| {'name': creator}}
  end

  def set_contributors
    document['contributors'] = [{'name': 'Duke University', 'nameType': 'Organizational', 'contributorType': 'HostingInstitution', nameIdentifiers: [{nameIdentifier: 'https://ror.org/00py81415', nameIdentifierScheme: 'ROR', schemeURI: 'https://ror.org/'}]}]
    document['contributors'] += work.contact.map{|contact| {'name': contact, 'contributorType': 'ContactPerson'}}
    document['contributors'] += work.contributor.map{|contributor| {'name': contributor, 'contributorType': 'RelatedPerson'}} if work.contributor.present?
  end

  def set_alternate_identifiers
    document['alternateIdentifiers'] = [ {'alternateIdentifier': work.ark, 'alternateIdentifierType': 'ARK'} ]
  end

  def set_related_identifiers
    if work.replaces.present? || work.is_replaced_by.present?
      document['relatedIdentifiers'] = []
      document['relatedIdentifiers'] << {'relatedIdentifier': work.replaces,
                                         'relatedIdentifierType': 'DOI',
                                         'relationType': 'IsNewVersionOf'} if work.replaces.present?
      document['relatedIdentifiers'] << {'relatedIdentifier': work.is_replaced_by,
                                         'relatedIdentifierType': 'DOI',
                                         'relationType': 'IsPreviousVersionOf'} if work.is_replaced_by.present?
    end
  end

  def set_publisher
    document['publisher'] = work.publisher.first.presence || 'Duke Research Data Repository'
  end

  def set_publication_year
    return if work.available.empty?

    work_date = work.available.first

    pubdate = nil
    if work_date =~ /\d{1,2}\/\d{1,2}\/\d{4}/
      pubdate = Date.strptime(work_date, '%m/%d/%Y')
    elsif work_date =~ /\d{1,2}\/\d{1,2}\/\d{2}/
      pubdate = Date.strptime(work_date, '%m/%d/%y')
    else
      pubdate = Date.parse(work_date)
    end
    document['publicationYear'] = pubdate.year unless pubdate.nil?
  rescue Date::Error => e
    Rails.logger.warn("Error parsing publication year from #{work.inspect} / #{work.available.inspect}: #{e.inspect}")
  end

  def set_types
    document['types'] = {'resourceTypeGeneral': work.resource_type&.first&.presence || 'Dataset' }
  end

  def set_doi
    document['doi'] = work.doi
  end

  def set_url
    document['url'] = resource_url
  end

  def set_funding_references
    document['fundingReferences'] = work.funding_agency.map {|funding_agency| { 'funderName': funding_agency } } if work.funding_agency.present?
  end

  def set_rights_list
    document['rightsList'] = work.license.map{|license| { 'rights': license } } if work.license.present?
  end

  def set_descriptions
    document['descriptions'] = work.description.map{|description| { 'description': description, 'descriptionType': 'Abstract'} } if work.description.present?
  end

  def set_dates
    available_dates = work.available.select{|available| available =~ /\d{4}-\d{2}-\d{2}/}.map{|available| {'date': available, 'dateType': 'Available'}}
    document['dates'] = available_dates if available_dates.present?
  end

  def set_subjects
    document['subjects'] = work.subject.map{|subject| {'subject': subject} } if work.subject.present?
  end

  def resource_url
    BASE_RESOURCE_URL + work.ark
  end
end
