module Rdr
  class RdrDerivativeService < Hyrax::DerivativeService
    class_attribute :services
    self.services = [Rdr::RdrFileSetDerivativesService]
  end
end
