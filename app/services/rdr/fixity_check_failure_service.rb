# frozen_string_literal: true

module Rdr
  class FixityCheckFailureService
    def self.call(file_set, checksum_audit_log:)
      transformed_checked_uri = checksum_audit_log.checked_uri.sub('http://fcrepo:8080/',"https://#{Rdr.host_name}/")
      metadata_endpoint = transformed_checked_uri + '/fcr:metadata'
      fixity_endpoint = transformed_checked_uri + '/fcr:fixity'
      error_msg = <<~EOS
        Research Data Repository fixity check failure
        FileSet #{file_set.id} failed fixity check.

        Details:
          file_set_id: #{checksum_audit_log.file_set_id}
          file_id: #{checksum_audit_log.file_id}
          stored checksum: #{file_set.original_file&.checksum&.uri}
          checksum returned by fixity service: #{checksum_audit_log.expected_result}
          fixity check link: #{fixity_endpoint}
          metadata link: #{metadata_endpoint}
          download link: #{transformed_checked_uri}
      EOS
      error = Rdr::ChecksumInvalid.new(error_msg)
      ChecksumVerificationMailer.notify_failure(error).deliver_now
    end
  end
end
