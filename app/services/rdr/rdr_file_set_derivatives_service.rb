module Rdr
  class RdrFileSetDerivativesService < Hyrax::FileSetDerivativesService
    private
      def create_office_document_derivatives(filename)
        extract_full_text(filename, uri)
      end
  end
end
