# frozen_string_literal: true

# Customized copy of Hyrax core file, modified in order to support
# more kinds of thumbnails (custom code noted inline).
# Works with DUL Custom overrides of Hydra::Works::MimeTypes:
# https://gitlab.oit.duke.edu/ddr/rdr/-/blob/main/lib/hydra/works/models/concerns/file_set/mime_types.rb
# https://github.com/samvera/hydra-works/blob/main/lib/hydra/works/models/concerns/file_set/mime_types.rb

# TODO: monitor how this evolves in Hyrax core w/r/t Hydra::Works. E.g., see:
# https://github.com/samvera/hyrax/commit/765132a5d7623764e2b4eccd097c7493f1c96c33
# https://github.com/samvera/hyrax/blob/main/app/services/hyrax/file_set_type_service.rb

# Last checked for updates: Hyrax 3.1.0. See:
# https://github.com/samvera/hyrax/blob/main/app/services/hyrax/thumbnail_path_service.rb

module Hyrax
  class ThumbnailPathService
    class << self
      # @param [Work, FileSet] object - to get the thumbnail for
      # @return [String] a path to the thumbnail
      def call(object)
        return default_image if object.try(:thumbnail_id).blank?

        thumb = fetch_thumbnail(object)
        return unless thumb
        return call(thumb) unless thumb.is_a?(::FileSet)

        if thumb.audio?
          audio_image
        # DUL CUSTOM THUMBS BY FILE TYPE
        elsif thumb.vector_file?
          vector_file_image
        elsif thumb.data_file?
          data_file_image
        elsif thumb.text_file?
          text_file_image
        elsif thumb.script_file?
          script_file_image
        # END DUL CUSTOM THUMBS BY FILE TYPE
        elsif thumbnail?(thumb)
          thumbnail_path(thumb)
        else
          default_image
        end
      end

      private

      def fetch_thumbnail(object)
        return object if object.thumbnail_id == object.id

        ::ActiveFedora::Base.find(object.thumbnail_id)
      rescue ActiveFedora::ObjectNotFoundError
        Rails.logger.error("Couldn't find thumbnail #{object.thumbnail_id} for #{object.id}")
        nil
      end

      # @return the network path to the thumbnail
      # @param [FileSet] thumbnail the object that is the thumbnail
      def thumbnail_path(thumbnail)
        Hyrax::Engine.routes.url_helpers.download_path(thumbnail.id,
                                                       file: 'thumbnail')
      end

      def default_image
        ActionController::Base.helpers.image_path 'default.png'
      end

      def audio_image
        ActionController::Base.helpers.image_path 'audio.png'
      end

      # DUL CUSTOM THUMBS BY FILE TYPE
      def vector_file_image
        ActionController::Base.helpers.image_path 'vector.png'
      end

      def data_file_image
        ActionController::Base.helpers.image_path 'data.png'
      end

      def text_file_image
        ActionController::Base.helpers.image_path 'text.png'
      end

      def script_file_image
        ActionController::Base.helpers.image_path 'script.png'
      end
      # END DUL CUSTOM THUMBS BY FILE TYPE

      # @return true if there a file on disk for this object, otherwise false
      # @param [FileSet] thumb - the object that is the thumbnail
      def thumbnail?(thumb)
        File.exist?(thumbnail_filepath(thumb))
      end

      # @param [FileSet] thumb - the object that is the thumbnail
      def thumbnail_filepath(thumb)
        Hyrax::DerivativePath.derivative_path_for_reference(thumb, 'thumbnail')
      end
    end
  end
end
