﻿This readme file was generated on [YYYY-MM-DD] by [NAME]

<Items preceded by an * can be deleted if not relevant> <help text in angle brackets should be deleted before finalizing your document>

-------------------
GENERAL INFORMATION
-------------------

Title of Dataset:
<If the data are associated with an article you would want to give the datset the same title e.g. Data from: Article title;
Data and scripts from: Article title>

Author Contact Information (Name, Institution, Email, ORCID)

	Principal Investigator:
	Institution:
	Email:
	ORCID: 

	Associate or Co-investigator:
	Institution:
	Email:
	ORCID: 

	Alternate Contact(s):

*Date of data collection (single date, range, approximate date): <suggested format YYYYMMDD>

*Geographic location of data collection (if applicable): <City, State, County, Country and/or GPS Coordinates or bounding boxes> 

*Funding and grant numbers (if applicable): 

--------------------
DATA & FILE OVERVIEW
--------------------

File list (filenames, directory structure (for zipped files) and brief description of all data files):

*Relationship between files, if important for context:  

*If data was derived from another source, list source citation:


--------------------------
METHODOLOGICAL INFORMATION
--------------------------

Description of methods used for collection/generation of data: 

*Software- or Instrument-specific information needed to interpret the data, including software version numbers, packages or other dependencies:

*Standards and calibration information, if appropriate: 

*Environmental/experimental conditions: 

--------------------------
DATA-SPECIFIC INFORMATION <Create sections for EACH data file or set, as appropriate>
--------------------------

Variable/field list
Define each including spelling out abbreviations

Value/attribute list
Include units of measure, codes or symbols used
   
Missing data treatments (null, -99, na, etc.)

-------------------------
USE and ACCESS INFORMATION <to be completed by RDR staff based upon information entered in metadata deposit form>
--------------------------

Data License:

Other Rights Information: 

To cite the data: 


