# frozen_string_literal: true

Role.create(name: 'admin')
Role.create(name: User::CURATOR_GROUP)

if Rails.env.development?
  admin_uid = ENV.fetch('RDR_ADMIN_USER', 'rdradmin')
  unless admin = User.find_by(uid: admin_uid)
    admin_pass = ENV.fetch('RDR_ADMIN_PASSWORD', 'rdradmin')
    role = Role.find_by(name: 'admin')
    User.create(uid: admin_uid,
                email: ENV.fetch('RDR_ADMIN_EMAIL', 'noreply@duke.edu'),
                password: admin_pass,
                password_confirmation: admin_pass,
                roles: [role])
  end
end
