# frozen_string_literal: true

class DropGlobusExportsSubscribers < ActiveRecord::Migration[5.2]
  def change
    remove_column :globus_exports, :subscribers, :text
  end
end
