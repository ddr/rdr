#!/bin/bash
#
# Entrypoint script for running rails tasks before container command.
#
# Terminate task list with '--'.
#
# Ex.
#
#   entrypoint:
#     - ./rails-entrypoint.sh
#     - db:setup
#     - db:migrate
#     - '--'
#   command:
#     - ./bin/rails
#     - server
#
tasks=()
for task; do
    shift

    if [[ $task == "--" ]]; then
	break
    else
	tasks+=("${task}")
    fi
done

set -x
./bin/rails ${tasks[*]}
set +x

exec "$@"
