# RDR: Research Data Repository application for Duke University Libraries

## Development Environment

### Requirements

- Docker Destop for Mac or Windows OR docker-ce + docker-compose for Linux

### Building the application image

In the root of the project run:

	$ make

### Start the development stack

Start the development stack:

    $ .docker/dev.sh up

(If the app image is not present, it will be built
before the stack comes up.)

To initialize the development database for Rails, run the command:

    $ .docker/dev.sh exec app bin/rails db:setup

To run a Bash shell on the development app server:

    $ .docker/dev.sh exec app bash

Stop the development stack:

    $ .docker/dev.sh down

(If you use `Ctrl-C`to interrupt the stack running in the foreground, `dev.sh`
has a trap to run `docker-compose down` to clean up resources.)

## Test Environment

To get an interactive shell with the test stack, run:

    $ .docker/test-interactive.sh

This command will start the app container with the local code directory
bind-mounted, reset the database, and start an interactive bash session.

After exiting the bash session, the script will
cleanup the test environment, removing the containers and network.

To exercise the full test suite *against the latest build* -- i.e., *not*
including uncommitted changes -- run:

    $ make clean test

(This is mainly for CI pipelines, FWIW.)

If you have made changes since the last build and want to run the test suite
against your local code without doing another build, then use the interactive
mode below and run the command `bundle exec rake` or `bin/rails spec`  as usual
in the container.

## Environment Variables

The `app` container has a dedicated environment file at `.docker/app.env`.
Variables set in this file are propagated into the `app` container at runtime.

Some variables must be exported on the development host:

    EZID_TEST_PASSWORD

Additional variables can *optionally* be exported to override defaults:

	CURATION_GROUP_EMAIL
	DEFAULT_FROM_ADDRESS
	EXPORT_FILES_CONTACT_EMAIL
