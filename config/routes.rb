# frozen_string_literal: true

Rails.application.routes.draw do
  concern :oai_provider, BlacklightOaiProvider::Routes.new

  concern :range_searchable, BlacklightRangeLimit::Routes::RangeSearchable.new
  concern :searchable, Blacklight::Routes::Searchable.new
  mount Riiif::Engine => 'images', as: :riiif if Hyrax.config.iiif_image_server?
  mount Blacklight::Engine => '/'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/queues'

  resource :catalog, only: [:index], as: 'catalog', path: '/catalog', controller: 'catalog' do
    concerns :oai_provider

    concerns :searchable
    concerns :range_searchable
  end

  # The order of the following routes is important, as the first match wins.
  # placing this higher up in the routes file will cause it to take precedence
  scope module: 'hyrax', path: 'concern' do
    scope path: 'parent/:parent_id' do
      resources :file_sets, only: [:show], controller: 'rdr_file_sets'
    end
    resources :file_sets, only: [:show], controller: 'rdr_file_sets'
  end

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  mount Hydra::RoleManagement::Engine => '/'
  mount Qa::Engine => '/authorities'
  mount Hyrax::Engine, at: '/'
  resources :welcome, only: 'index'
  root 'hyrax/homepage#index'
  curation_concerns_basic_routes
  concern :exportable, Blacklight::Routes::Exportable.new

  resources :solr_documents, only: [:show], path: '/catalog', controller: 'catalog' do
    concerns :exportable
  end

  resources :bookmarks do
    concerns :exportable

    collection do
      delete 'clear'
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :hyrax, path: :concern do
    namespaced_resources 'datasets' do
      member do
        post :assign_register_doi
      end
    end
  end

  # Export Files (ZIP download)
  get 'export_files/:id', to: 'export_files#verify_export_login', as: 'verify_exportable'
  post 'export_files/:id', to: 'export_files#verify_export_login'
  post 'export_files/:id/verify_export_recaptcha', to: 'export_files#verify_export_recaptcha'

  resources :batch_imports, only: %i[new create]

  get 'id/*local_url_id', to: 'local_urls#show'

  resources :submissions, only: %i[new create]

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :status, only: :index
    end
  end
end
