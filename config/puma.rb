# frozen_string_literal: true

threads_count = ENV.fetch('RAILS_MAX_THREADS', 5)
threads threads_count, threads_count
plugin :tmp_restart
bind 'tcp://0.0.0.0:3000'
