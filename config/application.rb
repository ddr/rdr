# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Rdr
  class Application < Rails::Application
    config.load_defaults 5.1
    config.autoload_paths += %W[#{config.root}/lib]
    config.colorize_logging = false
    config.active_job.queue_adapter = :inline

    config.log_formatter = ::Logger::Formatter.new
    logger               = ActiveSupport::Logger.new($stdout)
    logger.formatter     = config.log_formatter
    config.logger        = ActiveSupport::TaggedLogging.new(logger)
    config.log_level     = ENV.fetch('RAILS_LOG_LEVEL', 'info').to_sym

    # Inject/override behaviors in existing classes without having to override the entire class.
    # Docs: http://samvera.github.io/patterns-presenters.html#overriding-and-custom-presenter-methods
    config.to_prepare do
      Hyrax::PermissionBadge.prepend PrependedPresenters::PermissionBadge
    end
  end
end
