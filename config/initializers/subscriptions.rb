# frozen_string_literal: true

ActiveSupport::Notifications.subscribe(Rdr::Notifications::FILE_INGEST_FINISHED, ChecksumVerificationService)
