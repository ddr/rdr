# frozen_string_literal: true

# Monkey-patched extensions to Hyrax core form:
# https://github.com/samvera/hyrax/blob/main/app/forms/hyrax/forms/collection_form.rb

# Last checked for updates: Hyrax 3.1.0

# NOTE: this line gets around NameError: uninitialized constant Hyrax::SearchState
# that is otherwise thrown when patching CollectionForm.
require Hyrax::Engine.root.join('lib', 'hyrax', 'search_state.rb')

Hyrax::Forms::CollectionForm.class_eval do
  delegate :affiliation, :ark, :available, :contact, :doi, :format, :funding_agency,
           :grant_number, :is_replaced_by, :provenance, :replaces, :rights_notes, :temporal,
           :alternative_title, :bibliographic_citation, :resource_type, :title, :creator,
           :contributor, :description, :abstract, :keyword, :license, :rights_notes,
           :rights_statement, :access_right, :publisher, :date_created, :subject, :language,
           :representative_id, :thumbnail_id, :identifier, :based_near, :related_url,
           :visibility, :collection_type_gid,
           to: :model

  # All terms in the form; See:
  # https://github.com/samvera/hyrax/blob/master/app/forms/hyrax/forms/collection_form.rb#L25-L28
  # ----------------------------------------
  def self.terms
    hyrax_core_terms = %i[
      alternative_title
      bibliographic_citation
      resource_type
      title
      creator
      contributor
      description
      abstract
      keyword
      license
      rights_notes
      rights_statement
      access_right
      publisher
      date_created
      subject
      language
      representative_id
      thumbnail_id
      identifier
      based_near
      related_url
      visibility
      collection_type_gid
    ]

    # DUL CUSTOM COLLECTION PROPERTIES
    # See: app/models/concerns/rdr/metadata.rb
    # ----------------------------------------
    hyrax_core_terms + %i[
      affiliation
      ark
      available
      contact
      doi
      format
      funding_agency
      grant_number
      is_replaced_by
      provenance
      replaces
      temporal
    ]
  end

  # Fields that are collapsed by default
  # behind an Additional Fields button. See:
  # https://github.com/samvera/hyrax/blob/master/app/forms/hyrax/forms/collection_form.rb#L63-L78
  # ----------------------------------------
  def secondary_terms
    hyrax_core_terms = %i[
      abstract
      alternative_title
      bibliographic_citation
      creator
      contributor
      keyword
      license
      rights_notes
      rights_statement
      access_right
      publisher
      date_created
      subject
      language
      identifier
      based_near
      related_url
      resource_type
    ]

    # DUL CUSTOM COLLECTION PROPERTIES
    # See: app/models/concerns/rdr/metadata.rb
    # ----------------------------------------
    hyrax_core_terms + %i[
      affiliation
      ark
      available
      contact
      doi
      format
      funding_agency
      grant_number
      is_replaced_by
      provenance
      replaces
      temporal
    ]
  end
end
