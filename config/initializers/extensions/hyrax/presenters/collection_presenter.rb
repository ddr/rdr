# frozen_string_literal: true

# Monkey-patched extensions to Hyrax core presenter:
# https://github.com/samvera/hyrax/blob/master/app/presenters/hyrax/collection_presenter.rb

# Last checked for updates: Hyrax 3.1.0

Hyrax::CollectionPresenter.class_eval do
  # Have to delegate to SolrDocument any fields we're using that aren't already delegated in
  # https://github.com/samvera/hyrax/blob/main/app/presenters/hyrax/collection_presenter.rb#L37-L41
  delegate :abstract, :alternative_title, :bibliographic_citation, :resource_type,
           :rights_notes, :rights_statement, :access_right, :based_near, :based_near_label,
           :affiliation, :ark, :available, :contact, :doi, :format, :funding_agency,
           :grant_number, :is_replaced_by, :provenance, :replaces, :temporal,
           to: :solr_document

  # List of fields in _show_descriptions.html.erb; See:
  # https://github.com/samvera/hyrax/blob/main/app/presenters/hyrax/collection_presenter.rb#L42-L47
  # ----------------------------------------
  def self.terms
    hyrax_core_terms = %i[
      abstract
      alternative_title
      bibliographic_citation
      total_items
      size
      resource_type
      creator
      contributor
      keyword
      license
      rights_notes
      rights_statement
      access_right
      publisher
      date_created
      subject
      language
      identifier
      based_near
      related_url
    ]

    # DUL CUSTOM COLLECTION PROPERTIES
    # See: app/models/concerns/rdr/metadata.rb
    # ----------------------------------------
    hyrax_core_terms + %i[
      affiliation
      ark
      available
      contact
      doi
      format
      funding_agency
      grant_number
      is_replaced_by
      provenance
      replaces
      temporal
    ]
  end
end
