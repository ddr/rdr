# frozen_string_literal: true

# Monkey-patched extensions to Hyrax core presenter:
# https://github.com/samvera/hyrax/blob/master/app/presenters/hyrax/file_set_presenter.rb

# Last checked for updates: Hyrax 2.9.0
include ActiveSupport::NumberHelper

Hyrax::FileSetPresenter.class_eval do
  def file_size
    solr_document['file_size_lts'].to_i || 0
  end

  def file_size_humanized
    ActiveSupport::NumberHelper.number_to_human_size(file_size)
  end

  def file_globus_download_btn_classes
    classes = %w[btn btn-default btn-download]
    classes << 'disabled' unless parent.globus_ready?
    classes
  end

  def file_globus_download_btn_tooltip
    tooltip = I18n.t('hyrax.file_sets.actions.download_from_globus_tooltip', filesize: file_size_humanized)
    if parent.globus_processing?
      tooltip << [' ', I18n.t('hyrax.file_sets.actions.download_from_globus_tooltip_processing')].join
    end
    tooltip
  end

  private

  # Change from Hyrax's WorkShowPresenter to DUL-Custom DatasetPresenter
  # This gets us access to the parent Dataset, esp. for a link to Globus
  # for large file download

  def fetch_parent_presenter
    ids = ActiveFedora::SolrService.query("{!field f=member_ids_ssim}#{id}",
                                          fl: ActiveFedora.id_field)
                                   .map { |x| x.fetch(ActiveFedora.id_field) }
    Hyrax::PresenterFactory.build_for(ids: ids,
                                      presenter_class: Hyrax::DatasetPresenter,
                                      presenter_args: current_ability).first
  end
end
