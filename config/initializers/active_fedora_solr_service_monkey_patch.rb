# frozen_string_literal: true

#
# Monkey patches ActiveFedora::SolrService.query
# to set default HTTP request method to POST.
#
# Also sets default rows to 10 to avoid this warning:
#   WARN -- : Calling ActiveFedora::SolrService.get without
#   passing an explicit value for ':rows' is not recommended.
#   You will end up with Solr's default (usually set to 10)
# See:
# https://github.com/samvera/active_fedora/blob/main/lib/active_fedora/solr_service.rb#L52-L67

module ActiveFedora
  class SolrService
    class << self
      alias _query query

      def query(q, args = {})
        args[:method] ||= :post
        args[:rows] ||= 10
        _query(q, args)
      end
    end
  end
end
