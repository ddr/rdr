# frozen_string_literal: true

Sidekiq.configure_server do |config|
  config.logger.level = ENV.fetch('SIDEKIQ_LOG_LEVEL', 'info').to_sym
end