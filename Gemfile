# frozen_string_literal: true

source 'https://rubygems.org'

ruby File.read(File.expand_path('../.ruby-version', __FILE__))

gem 'awesome_print'
gem 'bagit', git: 'https://github.com/duke-libraries/bagit.git', branch: 'ruby-3.3'
gem 'blacklight_oai_provider'
gem 'blacklight_range_limit', '~> 8', '< 8.3.0'
gem 'bootstrap', '~> 4.0'
gem 'boxr'
gem 'browse-everything'
gem 'coffee-rails', '~> 5.0'
gem 'ddr-antivirus'
gem 'devise', '>= 4.7.1'
gem 'devise-guests', '~> 0.8'
gem 'edtf-humanize'
gem 'equivalent-xml'
gem 'ezid-client'
gem 'hydra-role-management'
gem 'hyrax', git: 'https://github.com/duke-libraries/hyrax.git', branch: 'hyrax-5.0.4-rdr-hotfixes'
gem 'jquery-rails'
gem 'mini_magick', '>= 4.9.4'
gem 'net-imap', '>= 0.5.6'
gem 'nokogiri'
gem 'omniauth', '< 2'
gem 'omniauth-shibboleth'
gem 'pg'
gem 'puma', '~> 5.6.8'
gem 'rack', '>= 2.2.12'
gem 'rails', '~> 6.1.0'
gem 'recaptcha'
gem 'redis'
gem 'riiif', '~> 2.4'
gem 'rsolr', '>= 1.0'
gem 'sass-rails', '~> 6.0'
gem 'sidekiq', '< 7' # sidekiq 7 only supports Rails 6+
gem 'sidekiq-failures'
gem 'solrizer'
gem 'turbolinks', '~> 5'
gem 'twitter-typeahead-rails', '0.11.1.pre.corejavascript'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'uglifier', '>= 1.3.0'
gem 'uri', '>= 1.0.3'
gem 'workflow-activerecord', '>= 4.1', '< 6.0'
gem 'zip_tricks', '~> 5.6'

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'letter_opener'
  gem 'listen', '>= 3.0.5', '< 3.8'
  gem 'web-console', '>= 3.3.0'
  gem "debug"
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara'
  gem 'factory_bot_rails', '~> 6.2'
  gem 'rails-controller-testing'
  gem 'rspec-its'
  gem 'rspec-rails', '~> 4.0'
  gem 'rubocop-performance', '~> 1.14'
  gem 'rubocop-rails', '~> 2.15'
  gem 'rubocop-rspec', '~> 2.11'
  gem 'selenium-webdriver'
  gem 'webmock'
end
