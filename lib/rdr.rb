# frozen_string_literal: true

require 'rdr/version'
require 'rdr/error'

module Rdr
  extend ActiveSupport::Autoload

  autoload :Configurable
  autoload :Index
  autoload :Notifications
  autoload :FixityCheckJobExtensions

  include Rdr::Configurable

  def self.readable_date(value)
    Date.parse(value).to_formatted_s(:standard)
  rescue StandardError
    value
  end
end
