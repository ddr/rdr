# frozen_string_literal: true
module Rdr
  module FixityCheckJobExtensions
    extend ActiveSupport::Concern

    included do
      [Faraday::TimeoutError, Net::ReadTimeout].each do |error_class|
        discard_on(error_class) do |job, error|
          Rails.logger.warn("#{job.inspect}: #{error.inspect}")
        end
      end

      def perform(uri, file_set_id:, file_id:)
        Rails.logger.info("FixityCheckJobExtensions.perform called with (#{uri}, #{file_set_id.inspect}, #{file_id}) - fixity now handled by ingest check (ChecksumVerificationService) and FileTracker")
      end
    end
  end
end
