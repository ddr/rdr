# frozen_string_literal: true

module Rdr
  module Configurable
    extend ActiveSupport::Concern

    included do
      # reason recorded in Ezid when an ARK is made unavailable due to object deaccession
      mattr_accessor :deaccession_reason do
        ENV['DEACCESSION_REASON'] || 'Deaccessioned from repository'
      end

      # default from address for emails sent by application
      mattr_accessor :default_from_address do
        ENV['DEFAULT_FROM_ADDRESS'] || 'noreply@duke.edu'
      end

      # URL of depositor modification request form
      mattr_accessor :depositor_request_form do
        ENV['DEPOSITOR_REQUEST_FORM']
      end

      # Email verification token lifespan as an ActiveSupport::Duration
      # Currently used only in relation to Export Files functionality
      mattr_accessor :email_verification_token_lifespan do
        if value = ENV['EMAIL_VERIFICATION_TOKEN_LIFESPAN']
          eval(value)
        else
          48.hours
        end
      end

      # Contact email for export files
      mattr_accessor :export_files_contact_email do
        ENV['EXPORT_FILES_CONTACT_EMAIL'] || 'from@example.com'
      end

      # Source-Organization for export files bag info
      mattr_accessor :export_files_source_organization do
        ENV['EXPORT_FILES_SOURCE_ORGANIZATION'] || 'Duke University Libraries'
      end

      # File size limit in bytes for using the Fedora-based fixity check service
      # Default: 100GB
      mattr_accessor :fixity_check_size_limit do
        ENV['FIXITY_CHECK_SIZE_LIMIT']&.to_i || 100_000_000_000
      end

      # Host name for use in non-web-request situations
      mattr_accessor :host_name do
        ENV['HOST_NAME']
      end

      mattr_accessor :doi_prefix do
        ENV.fetch('DOI_PREFIX', '10.82288')
      end

      # DataCite login_id
      mattr_accessor :datacite_user do
        ENV['DATACITE_USER'] || 'NOT_SET'
      end

      # DataCite login_password
      mattr_accessor :datacite_password do
        ENV['DATACITE_PASSWORD'] || 'NOT_SET'
      end

      # DataCite registration host
      mattr_accessor :datacite_host do
        ENV['DATACITE_HOST'] || 'api.test.datacite.org'
      end

      # DataCite provider id
      mattr_accessor :datacite_provider_id do
        ENV['DATACITE_PROVIDER_ID'] ||
          ENV['DATACITE_USER']&.split('.')&.first&.downcase ||
          'gmhr'
      end

      # Geonames user account
      mattr_accessor :geonames_user do
        ENV['GEONAMES_USER'] || 'NOT_SET'
      end

      # The number of words (space-delimited) at which to collapse the display of
      # a long metadata value; click a Read More link to expand.
      # E.g., Description field on a work show page.
      mattr_accessor :expandable_text_word_cutoff do
        ENV.fetch('EXPANDABLE_TEXT_WORD_CUTOFF', 105).to_i
      end

      # The base Box folder for RDR Submissions
      mattr_accessor :box_base_folder_rdr_submissions do
        ENV['BOX_BASE_FOLDER_RDR_SUBMISSIONS'] || 'NOT_SET'
      end

      # The base URL for accessing the Box instance used for RDR Submissions
      mattr_accessor :box_base_url_rdr_submissions do
        ENV['BOX_BASE_URL_RDR_SUBMISSIONS'] || 'NOT_SET'
      end

      # The email address for the curation group
      mattr_accessor :curation_group_email do
        ENV['CURATION_GROUP_EMAIL'] || 'curators@example.org'
      end

      # Value used in Importer manifest to separate multiple values in a single
      # CSV cell.
      mattr_accessor :csv_mv_separator do
        ENV['CSV_MV_SEPARATOR'] || '|'
      end

      # How large (in GB) can a dataset be before we deem it too big for native export,
      # thus only permitting downloads via Globus?
      mattr_accessor :globus_only_gb_threshold do
        ENV.fetch('GLOBUS_ONLY_GB_THRESHOLD', '3').to_i
      end

      mattr_accessor :globus_base_url do
        'https://app.globus.org/file-manager'
      end

      # The UUID of the Globus "collection" used for export
      mattr_accessor :globus_export_origin_id do
        ENV['GLOBUS_EXPORT_ORIGIN_ID']
      end

      # This value should be set to the host directory path
      # for Globus exports.
      mattr_accessor :globus_export_directory do
        ENV['GLOBUS_EXPORT_DIRECTORY']
      end

      mattr_accessor :globus_export_readme_filename do
        ENV.fetch('GLOBUS_EXPORT_README_FILENAME', 'Duke_RDR_Export--README.txt')
      end

      mattr_accessor :globus_export_manifest_filename do
        ENV.fetch('GLOBUS_EXPORT_MANIFEST_FILENAME', 'Duke_RDR_Export--MANIFEST-SHA1.txt')
      end
    end

    module ClassMethods
      def configure
        yield self
      end
    end
  end
end
