# frozen_string_literal: true

require 'yaml'

module Rdr
  VERSION = YAML.load_file(File.expand_path('../../version.yml', __dir__))['variables']['VERSION']
end
