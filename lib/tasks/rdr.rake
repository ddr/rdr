# frozen_string_literal: true

require 'rdr'

namespace :rdr do
  desc "Tag version #{Rdr::VERSION} and push to GitHub."
  task tag: :environment do
    tag = "v#{Rdr::VERSION}"
    comment = "RDR #{tag}"
    system('git', 'push', 'origin', tag) if system('git', 'tag', '-a', tag, '-m', comment)
  end

  desc 'Refresh access and refresh tokens for Box Submissions'
  task refresh_box_tokens: :environment do
    Submissions::BoxClient.refresh_tokens
  end

  desc 'Fixity check FileSets without recent fixity checks'
  task check_fixity: :environment do
    Hyrax::RepositoryFixityCheckService.fixity_check_everything
  end

  desc 'Delete expired email verification tokens'
  task delete_expired_email_verification_tokens: :environment do
    EmailVerification.where("updated_at < '#{Time.zone.now - Rdr.email_verification_token_lifespan}'").destroy_all
  end

  desc 'List DOIs assigned in RDR but missing in DataCite'
  task list_missing_dois: :environment do
    DataciteRegistration.list_missing_dois
  end

  desc 'Mint DOIs assigned in RDR but missing in DataCite'
  task mint_missing_dois: :environment do
    DataciteRegistration.mint_missing_dois
  end

  desc 'Mint or update all DOIs assigned in RDR in DataCite'
  task mint_or_update_all_datasets_with_dois: :environment do
    DataciteRegistration.mint_missing_dois
    DataciteRegistration.update_all_datasets_with_dois
  end

  desc 'Transform all DOIs on works to have the current DOI Prefix'
  task transform_doi_prefixes: :environment do
    AssignDoi.transform_doi_prefixes!
  end

  namespace :globus do
    desc 'Destroy a Globus export by dataset ID'
    task :destroy, [:dataset_id] => :environment do |_t, args|
      export = Globus::Export.find_by!(dataset_id: args.fetch(:dataset_id))
      export.destroy
      puts "Globus export destroyed: #{export.inspect}"
    end

    desc 'Export a dataset to Globus by dataset ID'
    task :export, [:dataset_id] => :environment do |_t, args|
      export = Globus::Export.call(args.fetch(:dataset_id), async: true)
      puts "Globus export enqueued: #{export.inspect}"
    end

    desc 'Export all datasets to Globus'
    task export_all: :environment do
      Globus::ExportAll.call
    end

    desc 'Refresh stale Globus export metadata'
    task refresh_metadata: :environment do
      Globus::RefreshMetadataJob.perform_later
    end

    desc 'Refresh all Globus export metadata (force update)'
    task refresh_all_metadata: :environment do
      Globus::RefreshMetadataJob.perform_later(force: true)
    end

    desc 'Reset (wipe) Globus export for re-exporting'
    task :reset, [:dataset_id] => :environment do |_t, args|
      export = Globus::Export.find_by!(dataset_id: args.fetch(:dataset_id))
      if export.can_reset?
        export.reset!
        puts "Globus export reset: #{export.inspect}"
      else
        puts "Globus export cannot be reset; you may need to destroy first, then export. (#{export.inspect})"
      end
    end
  end

  namespace :tasks do
    desc 'Iterate over a works child datasets and update their visibility'
    task :update_work_visibility, [:work_id, :visibility] => :environment do |_t, args|
      parent_work = args.fetch(:work_id)
      visibility = args.fetch(:visibility)
      RdrHelper.update_work_visibility(parent_work, visibility)
    end
  end
end
