SHELL = /bin/bash

build_tag ?= rdr

ruby_version ?= $(shell cat .ruby-version)

.PHONY: build
build:
	./build.sh

.PHONY: clean
clean:
	rm -rf ./tmp/*
	rm -f ./log/*.log

.PHONY: test
test: clean
	./.docker/test.sh up --exit-code-from app; \
		code=$$?; \
		./.docker/test.sh down; \
		exit $$code

.PHONY: lock
lock:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle lock

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) ./audit.sh

.PHONY: update
update:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle update $(args)

.PHONY: rubocop
rubocop:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle exec rubocop $(args)

.PHONY: autocorrect
autocorrect:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle exec rubocop -a
