#!/bin/bash

compose_cmd="docker compose -f docker-compose.yml -f docker-compose.development.yml -p rdr-dev"

if [[ "$@" =~ up ]]; then
    trap "${compose_cmd} down" SIGINT

    if ! docker image ls | grep -q ^rdr-app ; then
	cd "$(git rev-parse --show-toplevel)"
	make
    fi
fi

cd "$(dirname ${BASH_SOURCE[0]})"

${compose_cmd} "$@"
