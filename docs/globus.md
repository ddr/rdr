# RDR-Globus integration

## Export to Globus

The new components added to the project to support Globus export include:

  - An ActiveRecord model: [Globus::Export](../app/models/globus/export.rb)
  - [Templates and partials](../app/views/globus)
  - A background job: [Globus::ExportJob](../app/jobs/globus/export_job.rb)
  - A new dependency to support workflow: [workflow-activerecord](https://github.com/geekq/workflow-activerecord)

The model consists of two attributes plus timestamps:

  - `dataset_id` - The ID of the exported dataset. There is a uniqueness constraint
	on this attribute to prevent multiple exports of the same dataset.
  - `workflow_state` - A term denoting the current state of the export process
	(details below).

To retrieve an export record associated with a dataset:

```ruby
# dataset = Dataset or SolrDocument

Globus::Export.find_by(dataset_id: dataset.id)
```

Some helper classes collaborate with `Globus::Export`:

  - [Globus::ExportBuilder](../app/models/globus/export_builder.rb) - Downloads
	the dataset files, assembles them with metadata, and verifies their integrity.
  - [Globus::ExportMetadata](../app/models/globus/export_metadata.rb) - Provides
	the appropriate metadata from the dataset to the export.

The workflow consists of three ordered states and transitions between them:

  - `new` - Initial state.  The export is building or waiting in a job queue to begin building.
  - `built` - The export has been "built", i.e., all files and metadata have been downloaded.
  - `exported` - The export has been moved to the Globus "collection" where it
	can be retrieved by end users through Globus.

To test whether an export is in a particular state, use the special methods:

```ruby
export = Globus::Export.find_by(dataset_id: dataset.id)

# Is it (still) building?
export.new?

# Is the build complete (but it's not yet staged)?
export.built?

# Is it ready to retrieve from Globus?
export.exported?
```

Transitions between the states are logged.

There is also a special transition, `reset`,which returns the export to its initial state.
**Note** This is a destructive action - i.e., any and all files associated with the export in the
build or staging areas are *deleted*.
