ARG ruby_version="3.3.4"
ARG builder="builder"
ARG bundle="bundle"

FROM ruby:${ruby_version} AS builder

ARG node_version="16"

SHELL ["/bin/bash", "-c"]

ENV APP_ROOT="/opt/app-root" \
    APP_USER="app-user" \
    APP_UID="1001" \
    APP_GID="0" \
    BUNDLE_IGNORE_CONFIG="true" \
    BUNDLE_USER_HOME="${GEM_HOME}" \
    DATA_VOLUME="/data" \
    FITS_HOME="/opt/fits" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    RAILS_ENV="production" \
    RAILS_PORT="3000" \
    TZ="US/Eastern"

RUN set -eux; \
    mkdir -p /etc/apt/keyrings; \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg; \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${node_version}.x nodistro main" \
    | tee /etc/apt/sources.list.d/nodesource.list; \
    apt-get -y update; \
    apt-get -y install \
    clamdscan \
    ffmpeg \
    file \
    ghostscript \
    imagemagick-6.q16 \
    jq \
    libmagickcore-6.q16-6-extra \
    libpq-dev \
    libreoffice-common \
    mediainfo \
    netpbm \
    nodejs npm \
    openjdk-17-jre-headless \
    unzip \
    zip \
    ; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*; \
    npm install -g yarn

# ClamAV and ImageMagick configs
COPY ./etc/ /etc/

WORKDIR $APP_ROOT

#------------------------

FROM ${builder} AS bundle

COPY .ruby-version Gemfile Gemfile.lock ./

RUN gem install bundler -v "$(tail -1 Gemfile.lock | tr -d ' ')" && \
    bundle install --no-cache && \
    chmod -R g=u $GEM_HOME

#------------------------

FROM ${bundle} AS app

ARG build_date="1970-01-01T00:00:00Z"
ARG git_commit="0"

LABEL org.opencontainers.artifact.description="Duke University Libraries Research Data Repository application"
LABEL org.opencontainers.image.url="https://research.repository.duke.edu"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/ddr/rdr"
LABEL org.opencontainers.image.documentation="https://gitlab.oit.duke.edu/ddr/rdr"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.created="${build_date}"
LABEL org.opencontainers.image.revision="${git_commit}"
LABEL org.opencontainers.image.authors="Duke University Libraries (https://library.duke.edu)"

COPY --from=gitlab-registry.oit.duke.edu/devops/containers/fits-container:main \
	/usr/src/fits $FITS_HOME

COPY . .

RUN set -eux; \
	useradd -r -u $APP_UID -g $APP_GID -d $APP_ROOT -s /sbin/nologin $APP_USER; \
	mkdir -p -m 0775 $DATA_VOLUME; \
	mkdir -p -m 0775 tmp/pids; \
	chown -R $APP_UID:$APP_GID . $DATA_VOLUME

USER $APP_USER

RUN SECRET_KEY_BASE=1 ./bin/rails assets:precompile

VOLUME $DATA_VOLUME

EXPOSE $RAILS_PORT

CMD ["./bin/rails", "server"]
