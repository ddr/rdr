# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Globus::RefreshMetadataJob, globus: true do
  specify {
    expect(Globus::Export).to receive(:refresh_metadata).with(force: true)
    described_class.perform_later(force: true)
  }

  specify {
    expect(Globus::Export).to receive(:refresh_metadata).with(force: false)
    described_class.perform_later
  }
end
