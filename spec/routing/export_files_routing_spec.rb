# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'export files routing', type: :routing do
  let(:id) { '3484zg88m' }

  it 'has a verify exportable route' do
    route = { controller: 'export_files', action: 'verify_export_login', id: id }
    expect(get: "/export_files/#{id}").to route_to(route)
    expect(get: verify_exportable_path(id)).to route_to(route)
  end

  it 'has a verify export login route' do
    route = { controller: 'export_files', action: 'verify_export_login', id: id }
    expect(post: "export_files/#{id}").to route_to(route)
  end

  it 'has an verify export recaptcha route' do
    route = { controller: 'export_files', action: 'verify_export_recaptcha', id: id }
    expect(post: "export_files/#{id}/verify_export_recaptcha").to route_to(route)
  end
end
