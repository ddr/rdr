# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Display homepage' do
  describe 'recently uploaded datasets' do
    before do
      Hyrax::AdminSetCreateService.find_or_create_default_admin_set.id
      FactoryBot.create(:public_dataset, title: ['Analysis'])
      FactoryBot.create(:public_dataset, title: ['Discussion'], bibliographic_citation: ['Data Citation'])
    end

    it do
      visit '/'
      expect(page).to have_content(I18n.t('hyrax.homepage.recently_uploaded.title'))
      expect(page).not_to have_content('Depositor')
      expect(page).to have_content('Analysis')
      expect(page).to have_content('Data Citation')
      expect(page).not_to have_content('Discussion')
    end
  end

  describe 'Queues link' do
    describe 'when not logged in' do
      it 'is not present' do
        visit '/'
        expect(page).not_to have_link('Queues', href: '/queues')
      end
    end
    describe 'when logged in' do
      let(:user) { FactoryBot.create(:user) }
      before { sign_in user }
      after { Warden.test_reset! }

      describe 'as a non-curator' do
        before { allow_any_instance_of(User).to receive(:curator?) { false } }
        it 'is not present' do
          visit '/'
          expect(page).not_to have_link('Queues', href: '/queues')
        end
      end

      describe 'as a curator' do
        before { allow_any_instance_of(User).to receive(:curator?) { true } }
        it 'is present' do
          visit '/'
          expect(page).to have_link('Queues', href: '/queues')
        end
      end
    end
  end
end
