# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'OAI-PMH', type: :request do
  before do
    @work = FactoryBot.create(:public_dataset_with_public_files)
    service = DatasetIndexer.new(@work)
    solr_document = service.generate_solr_document
  end

  describe 'Identify' do
    it 'returns http success' do
      get "#{oai_catalog_path}?verb=Identify"
      expect(response.status).to eq(200)
      expect(response.body).to include('Duke Research Data Repository')
    end
  end

  describe 'ListRecords' do
    it 'returns http success' do
      get "#{oai_catalog_path}?verb=ListRecords&metadataPrefix=oai_dc"
      expect(response.status).to eq(200)
    end
  end

  describe 'ListIdentifiers' do
    it 'returns http success' do
      get "#{oai_catalog_path}?verb=ListIdentifiers&metadataPrefix=oai_dc"
      expect(response.status).to eq(200)
      expect(response.body).to include('<identifier>')
    end
  end

  describe 'ListMetadataFormats' do
    it 'returns http success' do
      get "#{oai_catalog_path}?verb=ListMetadataFormats"
      expect(response.status).to eq(200)
    end
  end

  describe 'GetRecord' do
    it 'returns http success' do
      get "#{oai_catalog_path}?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:research.repository.duke.edu:" + @work.id
      expect(response.status).to eq(200)
      expect(response.body).to include('<dc:title>')
    end
  end
end
