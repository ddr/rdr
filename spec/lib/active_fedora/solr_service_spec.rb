# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ActiveFedora::SolrService do
  describe 'Monkey patch of .query method' do
    it 'sets the :method option to :post' do
      expect(described_class).to receive(:_query).with('foo', { method: :post, rows: 10 })
      described_class.query('foo', { rows: 10 })
    end

    it 'sets the :rows option to 10 if unspecified' do
      expect(described_class).to receive(:_query).with('foo', { method: :post, rows: 10 })
      described_class.query('foo')
    end

    it 'passes through the :rows option if specified' do
      expect(described_class).to receive(:_query).with('foo', { method: :post, rows: 123 })
      described_class.query('foo', { rows: 123 })
    end
  end
end
