# frozen_string_literal: true

require 'rails_helper'
require 'importer'

RSpec.describe Importer::CSVParser do
  subject { described_class.new(file) }

  let(:first_record) { subject.first }

  context 'parsing metadata file' do
    let(:file) { "#{fixture_path}/importer/manifest_samples/sample.csv" }

    it 'parses a record' do
      expect(first_record[:title]).to eq ['Test 1']
      expect(first_record[:file]).to eq ['data/data1.csv', 'data/data2.csv', 'docs/doc1.txt']
      expect(first_record[:contributor]).to eq ['Smith, Sue', 'Jones, Bill', 'Allen, Jane']
      expect(first_record[:license].first).to be_a String
      expect(first_record[:license]).to eq ['http://creativecommons.org/publicdomain/zero/1.0/']
      expect(first_record[:visibility]).to eq ['open']
      expect(first_record.keys)
        .to match_array %i[ark visibility title contributor resource_type license file]
    end
  end

  context 'parsing UTF-8 metadata file with 0xEF 0xBB 0xBF BOM' do
    let(:file) { "#{fixture_path}/importer/manifest_samples/sample_bom_utf8_efbbbf.csv" }

    it 'parses a record' do
      expect(first_record[:title]).to eq ['Test 1']
      expect(first_record[:file]).to eq ['data/data1.csv', 'data/data2.csv', 'docs/doc1.txt']
      expect(first_record[:contributor]).to eq ['Smith, Sue', 'Jones, Bill', 'Allen, Jane']
      expect(first_record[:license].first).to be_a String
      expect(first_record[:license]).to eq ['http://creativecommons.org/publicdomain/zero/1.0/']
      expect(first_record[:visibility]).to eq ['open']
      expect(first_record.keys)
        .to match_array %i[ark visibility title contributor resource_type license file]
    end
  end
  
  context 'parsing UTF-8 metadata file with 0xFE 0xFF BOM' do
    let(:file) { "#{fixture_path}/importer/manifest_samples/sample_bom_utf8_feff.csv" }

    it 'raises a Encoding::InvalidByteSequenceError' do
      expect { first_record }.to raise_error(Encoding::InvalidByteSequenceError, 'incomplete "\n" on UTF-16BE')
    end
  end

  context 'parsing UTF-16 (BE) metadata file with 0xFE 0xFF BOM' do
    let(:file) { "#{fixture_path}/importer/manifest_samples/sample_bom_utf16_feff.csv" }

    it 'parses a record' do
      expect(first_record[:title]).to eq ['Test 1']
      expect(first_record[:file]).to eq ['data/data1.csv', 'data/data2.csv', 'docs/doc1.txt']
      expect(first_record[:contributor]).to eq ['Smith, Sue', 'Jones, Bill', 'Allen, Jane']
      expect(first_record[:license].first).to be_a String
      expect(first_record[:license]).to eq ['http://creativecommons.org/publicdomain/zero/1.0/']
      expect(first_record[:visibility]).to eq ['open']
      expect(first_record.keys)
        .to match_array %i[ark visibility title contributor resource_type license file]
    end

    it 'converts to UTF-8' do
      expect(first_record[:title].first.encoding).to eq Encoding::UTF_8
    end
  end

  context 'parsing UTF-16 (LE) metadata file with 0xFF 0xFE BOM' do
    let(:file) { "#{fixture_path}/importer/manifest_samples/sample_bom_utf16_fffe.csv" }

    it 'parses a record' do
      expect(first_record[:title]).to eq ['Test 1']
      expect(first_record[:file]).to eq ['data/data1.csv', 'data/data2.csv', 'docs/doc1.txt']
      expect(first_record[:contributor]).to eq ['Smith, Sue', 'Jones, Bill', 'Allen, Jane']
      expect(first_record[:license].first).to be_a String
      expect(first_record[:license]).to eq ['http://creativecommons.org/publicdomain/zero/1.0/']
      expect(first_record[:visibility]).to eq ['open']
      expect(first_record.keys)
        .to match_array %i[ark visibility title contributor resource_type license file]
    end

    it 'converts to UTF-8' do
      expect(first_record[:title].first.encoding).to eq Encoding::UTF_8
    end
  end


  describe '#headers' do
    let(:file) { "#{fixture_path}/importer/manifest_samples/sample.csv" }

    specify {
      expect(subject.headers)
        .to eq(%w[ark parent_ark visibility title contributor resource_type license file file file])
    }
  end

  describe '#parent_arks' do
    describe 'contains parent ARKs' do
      let(:file) { "#{fixture_path}/importer/manifest_samples/sample.csv" }

      specify { expect(subject.parent_arks).to match_array(['ark:/99999/fk4n02c87h']) }
    end

    describe 'does not contain parent ARKs' do
      let(:file) { "#{fixture_path}/importer/manifest_samples/basic.csv" }

      specify { expect(subject.parent_arks).to be_empty }
    end
  end
end
