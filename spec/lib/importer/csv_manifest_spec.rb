# frozen_string_literal: true

require 'rails_helper'

module Importer
  RSpec.describe CSVManifest do
    describe 'validation' do
      let(:manifest_file) { File.join(fixture_path, 'importer', 'manifest_samples', 'basic.csv') }
      let(:files_directory) { '/tmp' }

      subject { described_class.new(manifest_file, files_directory) }
      describe 'valid manifest' do
        before do
          allow(File).to receive(:exist?).and_call_original
          allow(File).to receive(:exist?).with(File.join(files_directory, 'data/data1.csv')).and_return(true)
          allow(File).to receive(:exist?).with(File.join(files_directory, 'data/data2.csv')).and_return(true)
          allow(File).to receive(:exist?).with(File.join(files_directory, 'data/data3.csv')).and_return(true)
          allow(File).to receive(:exist?).with(File.join(files_directory, 'docs/doc1.txt')).and_return(true)
          allow(File).to receive(:exist?).with(File.join(files_directory, 'docs/doc2.txt')).and_return(true)
        end

        it 'is valid' do
          expect(subject).to be_valid
        end
      end

      describe 'with "*" file value' do
        let(:manifest_file) do
          File.join(fixture_path, 'importer', 'manifest_samples', 'star.csv')
        end

        it 'is valid' do
          expect(subject).to be_valid
        end
      end

      describe 'invalid manifest' do
        describe 'invalid header' do
          let(:manifest_file) do
            File.join(fixture_path, 'importer', 'manifest_samples', 'invalid_headers.csv')
          end

          it 'has an invalid header error' do
            expect(subject).not_to be_valid
            expect(subject.errors.messages[:base])
              .to include(I18n.t('rdr.batch_import.invalid_metadata_header', header: 'bad_header'))
            expect(subject.errors.messages[:base])
              .to include(I18n.t('rdr.batch_import.invalid_metadata_header', header: 'ungood_header'))
          end
        end

        describe 'invalid byte sequence' do
          let(:manifest_file) do
            File.join(fixture_path, 'importer', 'manifest_samples', 'sample_bom_utf8_feff.csv')
          end

          it 'has an invalid byte sequence error' do
            expect(subject).not_to be_valid
            expect(subject.errors.messages[:base]).to include(I18n.t('rdr.batch_import.invalid_csv_byte_sequence',
                                                                     error: 'incomplete "\n" on UTF-16BE'))
          end
        end

        describe 'invalid controlled vocabulary metadata value' do
          let(:manifest_file) do
            File.join(fixture_path, 'importer', 'manifest_samples', 'invalid_controlled_vocab_values.csv')
          end

          it 'has an invalid metadata value error' do
            expect(subject).not_to be_valid
            expect(subject.errors.messages[:license])
              .to include(I18n.t('rdr.batch_import.invalid_metadata_value', value: 'bad_license'))
            expect(subject.errors.messages[:resource_type])
              .to include(I18n.t('rdr.batch_import.invalid_metadata_value', value: 'bad_resource_type'))
            expect(subject.errors.messages[:rights_statement])
              .to include(I18n.t('rdr.batch_import.invalid_metadata_value', value: 'bad_rights_statement'))
          end
        end

        describe 'file does not exist' do
          it 'has a file existence error' do
            expect(subject).not_to be_valid
            expect(subject.errors.messages[:files])
              .to include(I18n.t('rdr.batch_import.nonexistent_file',
                                 path: File.join(files_directory, 'data/data1.csv')))
          end
        end

        describe 'on behalf of user does not exist' do
          let(:manifest_file) do
            File.join(fixture_path, 'importer', 'manifest_samples', 'nonexistent_user.csv')
          end

          it 'has a nonexistent user error' do
            expect(subject).not_to be_valid
            expect(subject.errors.messages[:on_behalf_of]).to include(I18n.t('rdr.batch_import.nonexistent_user',
                                                                             user_key: 'abc@inst.edu'))
          end
        end

        describe 'malformed CSV manifest' do
          let(:error_msg) { 'malformed-ness of CSV manifest' }
          let(:exception_msg) { "#{error_msg} in line #{error_line}." }
          let(:error_line) { 4 }

          before do
            allow_any_instance_of(Importer::CSVParser).to receive(:as_csv_table).and_raise(
              CSV::MalformedCSVError.new(error_msg, error_line)
            )
          end

          it 'has a malformed CSV manifest error' do
            expect(subject).not_to be_valid
            expect(subject.errors.messages[:base]).to include(I18n.t('rdr.batch_import.malformed_csv_manifest',
                                                                     error: exception_msg))
          end
        end
      end
    end
  end
end
