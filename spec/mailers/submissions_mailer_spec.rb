# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SubmissionsMailer, type: :mailer do
  let(:submitter) { FactoryBot.create(:user, uid: 'user3@duke.edu', display_name: 'Rey Researcher') }
  let(:submission) { Submission.new(submission_attrs) }

  before do
    allow(Rdr).to receive(:curation_group_email).and_return('curators@example.org')
  end

  describe 'notify error' do
    let(:submission_attrs) { { submitter: submitter, deposit_agreement: Submission::AGREE } }
    let(:errors) { ActiveModel::Errors.new(submission) }

    before do
      errors.add(:title, :blank, message: "can't be blank")
      allow(submission).to receive(:errors) { errors }
    end

    it 'sends an appropriate email' do
      described_class.notify_error(submission).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([Rdr.curation_group_email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.submissions.email.error.subject'))
      expect(mail.body.encoded).to match("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to match("Net ID: #{submitter.netid}")
      expect(mail.body.encoded).to match("User Agent Info: #{submission_attrs[:user_agent]}")
      expect(mail.body.encoded).to match(I18n.t('rdr.submissions.email.error.message'))
      errors.full_messages.each do |msg|
        expect(mail.body.encoded).to match(msg)
      end
      expect(mail.body.encoded).to match("#{I18n.t('rdr.submissions.email.label.deposit_agreement')}: #{submission_attrs[:deposit_agreement]}")
    end
  end

  describe 'notify screened out' do
    let(:submission_attrs) { 
      { submitter: submitter, screening_pii: 'true',
        user_agent: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1' } 
    }

    it 'sends an appropriate email' do
      described_class.notify_screened_out(submission).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([Rdr.curation_group_email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.submissions.email.screened_out.subject'))
      expect(mail.body.encoded).to match("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to match("Net ID: #{submitter.netid}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.screening_pii')}: #{submission_attrs[:screening_pii]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.user_agent')}: #{submission_attrs[:user_agent]}")
      expect(mail.body.encoded).not_to match("#{I18n.t('rdr.submissions.email.label.screening_funding')}:")
    end
  end

  describe 'notify followup' do
    let(:submission_attrs) {
      { submitter: submitter, followup: 'true', more_information: 'here is some more information for you' }
    }

    it 'sends an appropriate email' do
      described_class.notify_followup(submission).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([Rdr.curation_group_email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.followup.email.subject'))
      expect(mail.body.encoded).to match("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to match("Net ID: #{submitter.netid}")
      expect(mail.body.encoded).to match(submission_attrs[:more_information].to_s)
    end
  end

  describe 'notify globus' do
    let(:submission_attrs) { { submitter: submitter, using_globus: 'true' } }

    it 'sends an appropriate email' do
      described_class.notify_globus(submission).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([Rdr.curation_group_email, submitter.email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.globus.email.subject'))
      expect(mail.body.encoded).to match("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to match("Net ID: #{submitter.netid}")
    end
  end

  describe 'notify success' do
    let(:submission_attrs) {
      { submitter: submitter, title: 'My Research Data Project', screening_pii: 'false',
        using_pace: 'false', creator: 'Spade, Sam; Tracy, Dick; Fletcher, Jessica',
        user_agent: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1' }
    }
    let(:submission_folder_id) { '1234567890' }
    let(:submission_folder_name) { "#{submitter.netid}_201804211423" }
    let(:submission_folder) {
      double('BoxrMash', etag: '0',  id: submission_folder_id, name: submission_folder_name,
                         type: 'folder')
    }
    let(:submission_folder_url) { "#{Rdr.box_base_url_rdr_submissions}/folder/#{submission_folder_id}" }

    before do
      allow(Rdr).to receive(:box_base_url_rdr_submissions).and_return('https://testbox.example.org')
    end

    it 'sends an appropriate email to submitter' do
      described_class.notify_success(submission, submission_folder).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([submitter.email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.submissions.email.success.subject'))
      expect(mail.body.encoded).to include(I18n.t('rdr.submissions.email.success.text',
                                                  email: Rdr.curation_group_email))
      expect(mail.body.encoded).to include("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to include("Net ID: #{submitter.netid}")
      expect(mail.body.encoded).to include("Submission Folder Name: #{submission_folder.name}")
      expect(mail.body.encoded).to include("Submission Folder URL: #{submission_folder_url}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.screening_pii')}: #{submission_attrs[:screening_pii]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.title')}: #{submission_attrs[:title]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.creator')}: #{submission_attrs[:creator]}")
      expect(mail.body.encoded).not_to include("#{I18n.t('rdr.submissions.email.label.contributor')}:")
    end

    it 'sends an appropriate email to curators' do
      described_class.notify_success_curators(submission, submission_folder).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([Rdr.curation_group_email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.submissions.email.success.subject'))
      expect(mail.body.encoded).to include("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to include("Net ID: #{submitter.netid}")
      expect(mail.body.encoded).to include("Submission Folder Name: #{submission_folder.name}")
      expect(mail.body.encoded).to include("Submission Folder URL: #{submission_folder_url}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.screening_pii')}: #{submission_attrs[:screening_pii]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.title')}: #{submission_attrs[:title]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.creator')}: #{submission_attrs[:creator]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.user_agent')}: #{submission_attrs[:user_agent]}")
      expect(mail.body.encoded).not_to include("#{I18n.t('rdr.submissions.email.label.contributor')}:")
    end
  end

  describe 'notify success using PACE' do
    let(:submission_attrs) {
      { submitter: submitter, title: 'My Research Data Project', screening_pii: 'false',
        using_pace: 'true', creator: 'Spade, Sam; Tracy, Dick; Fletcher, Jessica',
        user_agent: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1' }
    }
    let(:submission_folder_id) { '1234567890' }
    let(:submission_folder_name) { "#{submitter.netid}_201804211423" }
    let(:submission_folder) {
      double('BoxrMash', etag: '0',  id: submission_folder_id, name: submission_folder_name,
                         type: 'folder')
    }
    let(:submission_folder_url) { "#{Rdr.box_base_url_rdr_submissions}/folder/#{submission_folder_id}" }

    before do
      allow(Rdr).to receive(:box_base_url_rdr_submissions).and_return('https://testbox.example.org')
    end

    it 'sends an appropriate email to submitter' do
      described_class.notify_success(submission, submission_folder).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([submitter.email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.submissions.email.success_pace.subject'))
      expect(mail.body.encoded).to include(I18n.t('rdr.submissions.email.success_pace.text',
                                                  email: Rdr.curation_group_email))
      expect(mail.body.encoded).to include("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to include("Net ID: #{submitter.netid}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.screening_pii')}: #{submission_attrs[:screening_pii]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.title')}: #{submission_attrs[:title]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.creator')}: #{submission_attrs[:creator]}")
      expect(mail.body.encoded).not_to include("#{I18n.t('rdr.submissions.email.label.contributor')}:")
    end

    it 'sends an appropriate email to curators' do
      described_class.notify_success_curators(submission, submission_folder).deliver_now!
      mail = ActionMailer::Base.deliveries.last
      expect(mail.to).to match_array([Rdr.curation_group_email])
      expect(mail.from).to match_array([Rdr.curation_group_email])
      expect(mail.subject).to eq(I18n.t('rdr.submissions.email.success_pace.subject'))
      expect(mail.body.encoded).to include("Submitter: #{submitter.display_name}")
      expect(mail.body.encoded).to include("Net ID: #{submitter.netid}")
      expect(mail.body.encoded).to include("Submission Folder Name: #{submission_folder.name}")
      expect(mail.body.encoded).to include("Submission Folder URL: #{submission_folder_url}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.screening_pii')}: #{submission_attrs[:screening_pii]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.title')}: #{submission_attrs[:title]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.creator')}: #{submission_attrs[:creator]}")
      expect(mail.body.encoded).to include("#{I18n.t('rdr.submissions.email.label.user_agent')}: #{submission_attrs[:user_agent]}")
      expect(mail.body.encoded).not_to include("#{I18n.t('rdr.submissions.email.label.contributor')}:")
    end
  end
end
