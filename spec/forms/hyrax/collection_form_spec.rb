# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Hyrax::Forms::CollectionForm do
  let(:collection) { build(:collection_lw) }
  let(:ability) { Ability.new(create(:user)) }
  let(:repository) { double }
  let(:form) { described_class.new(collection, ability, repository) }

  describe '#terms' do
    subject { described_class.terms }

    before do
      allow(described_class).to receive(:model_class).and_return(Hyrax.config.collection_class)
    end

    it {
      expect(described_class.model_class).to eq(Hyrax.config.collection_class)
      expect(subject).to eq %i[
        alternative_title
        bibliographic_citation
        resource_type
        title
        creator
        contributor
        description
        abstract
        keyword
        license
        rights_notes
        rights_statement
        access_right
        publisher
        date_created
        subject
        language
        representative_id
        thumbnail_id
        identifier
        based_near
        related_url
        visibility
        collection_type_gid
        affiliation
        ark
        available
        contact
        doi
        format
        funding_agency
        grant_number
        is_replaced_by
        provenance
        replaces
        temporal
      ]
    }
  end

  describe '#secondary_terms' do
    subject { form.secondary_terms }

    before do
      allow(described_class).to receive(:model_class).and_return(Hyrax.config.collection_class)
    end

    it {
      expect(subject).to eq %i[
        abstract
        alternative_title
        bibliographic_citation
        creator
        contributor
        keyword
        license
        rights_notes
        rights_statement
        access_right
        publisher
        date_created
        subject
        language
        identifier
        based_near
        related_url
        resource_type
        affiliation
        ark
        available
        contact
        doi
        format
        funding_agency
        grant_number
        is_replaced_by
        provenance
        replaces
        temporal
      ]
    }
  end
end
