# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Globus::ExportAll, globus: true do
  after { Globus::Export.destroy_all }

  describe '.call' do
    let(:dataset) { FactoryBot.create(:public_dataset_with_public_files) }

    before do
      dataset.file_sets.each do |fs|
        Tempfile.open('foo') do |file|
          file.write SecureRandom.hex(100)
          file.rewind
          Hydra::Works::UploadFileToFileSet.call(fs, file)
        end
      end
    end

    xit 'exports unexported datasets' do
      expect_any_instance_of(Globus::Export).to receive(:build).and_call_original
      expect_any_instance_of(Globus::Export).to receive(:export).and_call_original

      described_class.call
    end

    describe 'with previously exported datasets' do
      before { described_class.call }

      it 'does not export them again' do
        expect_any_instance_of(Globus::Export).not_to receive(:build)
        expect_any_instance_of(Globus::Export).not_to receive(:export)
      end
    end

    describe 'with a reset export' do
      before do
        described_class.call
        Globus::Export.first.reset!
      end

      it 're-exports the dataset' do
        expect_any_instance_of(Globus::Export).to receive(:build)
        expect_any_instance_of(Globus::Export).to receive(:export)
        described_class.call
      end
    end
  end
end
