# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Globus::Export, globus: true do
  after { described_class.destroy_all }

  describe '.call' do
    let(:dataset) { FactoryBot.create(:public_dataset_with_public_files) }
    let(:export) { described_class.call(dataset) }
    let(:files) { [] }

    before do
      dataset.file_sets.each do |fs|
        Tempfile.open('foo') do |file|
          files << File.basename(file.path)
          file.write SecureRandom.hex(100)
          file.rewind
          Hydra::Works::UploadFileToFileSet.call(fs, file)
        end
      end
    end

    it 'exports the dataset (once)' do
      expect(export).to be_exported
      expect(File.exist?(export.export_path)).to be true
      expect(File.exist?(File.join(export.export_path, Rdr.globus_export_readme_filename))).to be true
      expect(File.exist?(File.join(export.export_path, Rdr.globus_export_manifest_filename))).to be true
      files.each do |file|
        expect(File.exist?(File.join(export.export_path, file))).to be true
      end
      expect_any_instance_of(described_class).not_to receive(:build)
      expect_any_instance_of(described_class).not_to receive(:export)
      described_class.call(dataset)
    end

    xit 'makes files read-only, not executable' do
      files.each do |path|
        file = File.join(export.export_path, path)
        expect(File.readable?(file)).to be true
        expect(File.writable?(file)).to be false
        expect(File.executable?(file)).to be false
      end
    end
  end

  describe 'stale?' do
    let(:dataset) do
      FactoryBot.create(:public_dataset_with_public_files).tap do |ds|
        ds.file_sets.each do |fs|
          Tempfile.open('foo') do |file|
            file.write SecureRandom.hex(100)
            file.rewind
            Hydra::Works::UploadFileToFileSet.call(fs, file)
          end
        end
      end
    end

    let(:export) { described_class.call(dataset) }

    xit 'is not stale at the outset' do
      expect(export).not_to be_stale
    end

    describe 'after metadata is updated' do
      before do
        export
        dataset.title = ['New title']
        dataset.save!
        ActiveFedora::SolrService.commit
      end

      xit 'is stale' do
        expect(export).to be_stale
      end
    end
  end

  describe '.refresh_metadata' do
    let(:datasets) do
      2.times.map do
        FactoryBot.create(:public_dataset_with_public_files).tap do |ds|
          ds.file_sets.each do |fs|
            Tempfile.open('foo') do |file|
              file.write SecureRandom.hex(100)
              file.rewind
              Hydra::Works::UploadFileToFileSet.call(fs, file)
            end
          end
        end
      end
    end

    let(:exports) { datasets.map { |ds| described_class.call(ds) } }

    before do
      exports
      datasets[0].title = ['New title']
      datasets[0].save!
    end

    it 're-exports the metadata file for stale exports' do
      readme0 = File.join(exports[0].export_path, Rdr.globus_export_readme_filename)
      readme1 = File.join(exports[1].export_path, Rdr.globus_export_readme_filename)

      expect { described_class.refresh_metadata; sleep 2 }.to change { File.mtime(readme0) }
      expect { described_class.refresh_metadata }.not_to change { File.mtime(readme1) }
    end

    xit 'makes the refreshed export not stale' do
      expect {
        described_class.refresh_metadata
        sleep 1
        exports[0].reload
      }.to change(exports[0], :stale?).from(true).to(false)
    end

    it "re-exports the metadata for all exports when `force: true' option is used" do
      readme0 = File.join(exports[0].export_path, Rdr.globus_export_readme_filename)
      readme1 = File.join(exports[1].export_path, Rdr.globus_export_readme_filename)

      expect { described_class.refresh_metadata(force: true); sleep 1 }.to change { File.mtime(readme0) }
      expect { described_class.refresh_metadata(force: true); sleep 1 }.to change { File.mtime(readme1) }
    end
  end

  describe 'run' do
    let(:dataset) do
      FactoryBot.create(:public_dataset_with_public_files).tap do |ds|
        ds.file_sets.each do |fs|
          Tempfile.open('foo') do |file|
            file.write SecureRandom.hex(100)
            file.rewind
            Hydra::Works::UploadFileToFileSet.call(fs, file)
          end
        end
      end
    end

    it 'can run asynchoronously' do
      expect(Globus::ExportJob).to receive(:perform_later)
      described_class.call(dataset, async: true)
    end
  end

  describe 'workflow' do
    subject { described_class.create(dataset_id: dataset.id) }

    let(:dataset) { FactoryBot.create(:public_dataset) }

    it { is_expected.to be_new }

    shared_examples 'Globus export reset workflow event' do
      describe 'reset' do
        before do
          allow_any_instance_of(described_class).to receive(:reset).and_return(nil)
          subject.reset!
        end

        it { is_expected.to be_new }
      end
    end

    describe 'build' do
      before do
        allow_any_instance_of(described_class).to receive(:build).and_return(nil)
        subject.build!
      end

      it { is_expected.to be_built }

      include_examples 'Globus export reset workflow event'

      describe 'export' do
        before do
          allow_any_instance_of(described_class).to receive(:export).and_return(nil)
          subject.export!
        end

        it { is_expected.to be_exported }

        include_examples 'Globus export reset workflow event'
      end
    end
  end
end
