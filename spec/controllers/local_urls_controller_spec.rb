# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LocalUrlsController, type: :controller do
  describe '#show' do
    let(:ark) { 'ark:/99999/fk45156v36' }

    context 'for dataset' do
      let!(:dataset) { FactoryBot.create(:dataset, ark: ark) }

      it 'redirects to dataset show page' do
        get :show, params: { local_url_id: ark }
        expect(response).to redirect_to(dataset)
      end
    end

    context 'for collection' do
      let!(:collection) do
        collection = build(:rdr_collection)
        collection.ark = ark
        allow(MintPublishArk).to receive(:call).and_return(nil)
        collection.save
        collection.reload
      end

      it 'redirects to collection show page' do
        get :show, params: { local_url_id: ark }
        expect(response).to redirect_to(collection)
      end
    end
  end
end
