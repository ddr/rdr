# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExportFilesController, type: :controller do
  let(:work) { FactoryBot.create(:dataset) }

  describe '#verify_export_login' do
    context 'login param is not present' do
      before do
        get :verify_export_login, params: { id: work.id }
      end

      it 'renders the unauthenticated template' do
        expect(response).to render_template(:unauthenticated)
      end

      it 'prompts to either login or verify the captcha' do
        expect(flash[:alert]).to eq(I18n.t('rdr.batch_export.verify.help'))
      end
    end

    context 'login param is present' do
      before do
        post :verify_export_login, params: { id: work.id, login: 1 }
      end

      it 'renders the login page' do
        expect(response.location).to match('users/sign_in')
      end

      it 'remembers to bring the user to the dataset page afterward' do
        expect(controller.stored_location_for(:user)).to eq(hyrax_dataset_path)
      end
    end
  end

  describe '#verify_export_recaptcha' do
    context 'when recaptcha was verified' do
      before do
        allow(controller).to receive(:verify_recaptcha).and_return(true)
        post :verify_export_recaptcha, params: { id: work.id }
      end

      it 'stores a session variable indicating the recaptcha was verified' do
        expect(session[:verified_recaptcha]).to be true
      end

      it 'renders the dataset page' do
        expect(response).to redirect_to(hyrax_dataset_path(work))
      end

      it 'displays a flash message that recaptcha verification succeeded' do
        expect(flash[:notice]).to eq(I18n.t('rdr.batch_export.verify.recaptcha_success'))
      end
    end

    context 'when recaptcha was not verified' do
      before do
        allow(controller).to receive(:verify_recaptcha).and_return(false)
        post :verify_export_recaptcha, params: { id: work.id }
      end

      it 'does not store a session variable indicating the recaptcha was verified' do
        expect(session[:verified_recaptcha]).to be nil
      end

      it 'renders the dataset page' do
        expect(response).to redirect_to(hyrax_dataset_path(work))
      end

      it 'displays a flash message that recaptcha verification failed' do
        expect(flash[:error]).to eq(I18n.t('rdr.batch_export.verify.recaptcha_fail'))
      end
    end
  end
end
