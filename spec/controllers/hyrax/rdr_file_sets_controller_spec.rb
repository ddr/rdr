# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Hyrax::RdrFileSetsController, type: :controller do
  let(:user) { FactoryBot.create(:user) }
  let(:file_set) { FactoryBot.create(:file_set) }
  let(:work) { FactoryBot.create(:dataset, title: ['test title'], user: user) }

  before do
    work.ordered_members << file_set
    work.save!
  end

  describe 'GET #show' do
    it 'routes GET concern/parent/{work.id}/file_sets/{file_set.id} to RdrFileSetsController#show' do
      expect(get: "/concern/parent/#{work.id}/file_sets/#{file_set.id}").to route_to(controller: 'hyrax/rdr_file_sets', action: 'show', id: file_set.id, parent_id: work.id)
      expect(controller.show_presenter).to eq(Hyrax::RdrFileSetPresenter)
    end
  end
end