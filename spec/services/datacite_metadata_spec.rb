# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DataciteMetadata do
  let(:work) {
    FactoryBot.build(:dataset,
                     title: ['DataCite Metadata Test'],
                     creator: ['Public, Jane Q.'],
                     contributor: ['Potter, Harry', 'Malfoy, Draco'],
                     available: ['2018-04-11'],
                     ark: 'ark:/99999/fk4zzzzz',
                     doi: '10.7924/fk4zzzzz')
  }

  describe '.call' do
    subject { described_class.call(work) }
  end
end
