# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MintPublishArk do
  subject { described_class.new(work) }

  let(:work) { FactoryBot.create(:dataset) }

  describe '#call' do
    describe 'ARK assignment' do
      before do
        allow(subject.ark).to receive(:target!).and_return(true)
      end

      describe 'no ARK assigned' do
        it 'assigns an ARK' do
          expect(subject.ark).to receive(:assign!)
          subject.call
        end
      end

      describe 'ARK already assigned' do
        before do
          allow(subject.ark).to receive(:assigned?).and_return(true)
          allow(subject.ark).to receive(:identifier) { double(reserved?: false, public?: true) }
        end

        it 'does not assign an ARK' do
          expect(subject.ark).not_to receive(:assign!)
          subject.call
        end
      end
    end

    describe 'set ARK target' do
      describe 'when no ARK assigned' do
        before do
          allow(subject.ark).to receive(:assigned?).and_return(false)
          stub_request(:post, 'https://ezid.cdlib.org/shoulder/ark:/99999/fk4')
            .with(
              body: "_profile: dc\n_export: no\n_status: reserved",
              headers: {
                'Accept' => 'text/plain',
                'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Authorization' => 'Basic YXBpdGVzdDphcGl0ZXN0',
                'Content-Type' => 'text/plain; charset=UTF-8',
                'Host' => 'ezid.cdlib.org',
                'User-Agent' => 'Ruby'
              }
            )
            .to_return(status: 201, body: 'success: ark:/99999/fk4b86j23c', headers: {})
        end

        it 'does not call the method to set a target' do
          expect(subject.ark).not_to receive(:target!)
          subject.call
        end
      end

      describe 'when ARK is assigned' do
        before do
          allow(subject.ark).to receive(:assigned?).and_return(true)
        end

        describe 'when the ARK is reserved' do
          before do
            allow(subject.ark).to receive(:identifier) { double(public?: false, reserved?: true) }
            allow(subject.ark).to receive(:publish!)
          end

          it 'calls the method to set a target' do
            expect(subject.ark).to receive(:target!)
            subject.call
          end
        end

        describe 'when the ARK is not reserved' do
          before do
            allow(subject.ark).to receive(:identifier) { double(public?: true, reserved?: false) }
          end

          it 'does not call the method to set a target' do
            expect(subject.ark).not_to receive(:target!)
            subject.call
          end
        end
      end
    end

    describe 'ARK publication' do
      before do
        allow(subject.ark).to receive(:target!).and_return(true)
      end

      describe 'no ARK assigned' do
        before do
          allow(subject.ark).to receive(:assign!)
        end

        it 'does not publish the ARK' do
          expect(subject.ark).not_to receive(:publish!)
          subject.call
        end
      end

      describe 'ARK assigned' do
        describe 'ARK not public' do
          before do
            allow(subject.ark).to receive(:assigned?).and_return(true)
            allow(subject.ark).to receive(:identifier) { double(reserved?: true, public?: false) }
          end

          it 'publishes the ARK' do
            expect(subject.ark).to receive(:publish!)
            subject.call
          end
        end

        describe 'ARK is public' do
          before do
            allow(subject.ark).to receive(:assigned?).and_return(true)
            allow(subject.ark).to receive(:identifier) { double(reserved?: false, public?: true) }
          end

          it 'does not publish the ARK' do
            expect(subject.ark).not_to receive(:publish!)
            subject.call
          end
        end
      end
    end
  end
end
