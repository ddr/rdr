# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DataciteRegistration do
  let(:work) {
    FactoryBot.build(:dataset,
                     title: ['DataCite Metadata Test'],
                     creator: ['Public, Jane Q.'],
                     contributor: ['Potter, Harry', 'Malfoy, Draco'],
                     available: ['2018-04-11'],
                     ark: 'ark:/99999/fk4zzzzz',
                     doi: '10.7924/fk4zzzzz')
  }

  describe '.call for a well-formed request' do
    before do
      stub_request(:post, 'https://api.test.datacite.org/dois').to_return(body: '', status: 201, headers: {})
    end

    it 'succeeds' do
      expect { described_class.call(work, dryrun: true) }.not_to raise_error
    end
  end

  describe '.call for a badly-formed request' do
    before do
      stub_request(:post, 'https://api.test.datacite.org/dois').to_return(body: '', status: 422, headers: {})
    end

    it 'raises an error for an unprocessable entity' do
      expect { described_class.call(work, dryrun: true) }.to raise_error(Rdr::DataciteRegistrationError)
    end
  end
end
