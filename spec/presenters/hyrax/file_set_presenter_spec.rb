# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Hyrax::FileSetPresenter do
  subject(:presenter) { described_class.new(solr_document, ability) }

  let(:solr_document) { SolrDocument.new }
  let(:dataset_solr_document) { SolrDocument.new }
  let(:request) { double(host: 'example.org') }
  let(:user) { FactoryBot.build(:user) }
  let(:ability) { Ability.new(user) }
  let(:dataset_presenter) { Hyrax::DatasetPresenter.new(dataset_solr_document, ability, request) }

  before do
    allow(subject).to receive(:parent) { dataset_presenter }
  end

  describe '#file_globus_download_btn_classes' do
    context 'dataset is not ready for Globus download' do
      before do
        allow(dataset_presenter).to receive(:globus_ready?).and_return(false)
      end

      it 'applies a disabled class' do
        expect(subject.file_globus_download_btn_classes).to include('disabled')
      end
    end

    context 'dataset is ready for Globus download' do
      before do
        allow(dataset_presenter).to receive(:globus_ready?).and_return(true)
      end

      it 'does not apply a disabled class' do
        expect(subject.file_globus_download_btn_classes).not_to include('disabled')
      end
    end
  end
end
