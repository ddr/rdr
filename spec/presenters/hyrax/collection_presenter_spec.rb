# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Hyrax::CollectionPresenter do
  describe '.terms' do
    subject { described_class.terms }

    it 'adds custom dataset-like fields to Hyrax collection fields' do
      expect(subject).to eq %i[
        abstract
        alternative_title
        bibliographic_citation
        total_items
        size
        resource_type
        creator
        contributor
        keyword
        license
        rights_notes
        rights_statement
        access_right
        publisher
        date_created
        subject
        language
        identifier
        based_near
        related_url
        affiliation
        ark
        available
        contact
        doi
        format
        funding_agency
        grant_number
        is_replaced_by
        provenance
        replaces
        temporal
      ]
    end
  end
end
