# frozen_string_literal: true

# Generated via
#  `rails generate hyrax:work Dataset`
require 'rails_helper'

RSpec.describe Hyrax::DatasetPresenter do
  subject { described_class.new(solr_document, ability, request) }

  let(:solr_document) { SolrDocument.new }
  let(:request) { double(host: 'example.org') }
  let(:user) { FactoryBot.build(:user) }
  let(:ability) { Ability.new(user) }
  let(:globus_export) { Globus::Export.new(dataset_id: 'abc123') }

  before do
    allow_any_instance_of(Collection).to receive(:mint_ark_for_collection).and_return(nil)
    allow(subject).to receive(:globus_export) { globus_export }
    %i{ark title creator available}.each do |required_attr|
      allow(subject).to receive(required_attr).and_return('present')
    end
  end

  describe '#doi_assignable?' do
    describe 'can assign/register DOIs' do
      before do
        ability.can :assign_register_doi, solr_document
      end

      it { is_expected.to be_assignable_doi }
    end

    describe 'cannot assign/register DOIs' do
      before do
        ability.cannot :assign_register_doi, solr_document
      end

      it { is_expected.not_to be_assignable_doi }
    end
  end

  describe '#small_dataset?' do
    before do
      allow(Rdr).to receive(:globus_only_gb_threshold).and_return(3)
      # NOTE: 3GB is 3,221,225,472 bytes
    end

    context 'total filesize is under the threshold' do
      before do
        allow(subject).to receive(:file_size_total).and_return(3_000_000_000)
      end

      it 'is considered a small dataset' do
        expect(subject.small_dataset?).to be true
      end
    end

    context 'total filesize is over the threshold' do
      before do
        allow(subject).to receive(:file_size_total).and_return(3_221_225_473)
      end

      it 'is not considered a small dataset' do
        expect(subject.small_dataset?).to be false
      end
    end
  end

  describe '#globus_url' do
    context 'export exists (has begun) and url exists' do
      before do
        allow(subject).to receive(:globus_export) { globus_export }
        allow(globus_export).to receive(:url_for)
          .and_return('https://app.globus.org/file-manager?origin_id=9&origin_path=%2Fabc123%2F')
      end

      it 'returns the url' do
        expect(subject.globus_url).to eq 'https://app.globus.org/file-manager?origin_id=9&origin_path=%2Fabc123%2F'
      end
    end

    context 'export does not exist (has not begun)' do
      before do
        allow(subject).to receive(:globus_export).and_return(nil)
      end

      it 'returns an empty hash' do
        expect(subject.globus_url).to eq '#'
      end
    end
  end

  describe '#export_btn_classes' do
    context 'not ready for Globus download (thus export)' do
      before do
        allow(subject).to receive(:globus_ready?).and_return(false)
      end

      it 'applies a disabled class' do
        expect(subject.export_btn_classes).to include('disabled')
      end
    end

    context 'ready for Globus download (thus export)' do
      before do
        allow(subject).to receive(:globus_ready?).and_return(true)
      end

      it 'does not apply a disabled class' do
        expect(subject.export_btn_classes).not_to include('disabled')
      end
    end
  end

  describe '#globus_download_btn_classes' do
    context 'not ready for Globus download' do
      before do
        allow(subject).to receive(:globus_ready?).and_return(false)
      end

      it 'applies a disabled class' do
        expect(subject.globus_download_btn_classes).to include('disabled')
      end
    end

    context 'ready for Globus download' do
      before do
        allow(subject).to receive(:globus_ready?).and_return(true)
      end

      it 'does not apply a disabled class' do
        expect(subject.globus_download_btn_classes).not_to include('disabled')
      end
    end
  end

  describe '#globus_processing?' do
    context 'export has not begun building' do
      before do
        allow(subject).to receive(:globus_export).and_return(nil)
      end

      it 'is not considered in process' do
        expect(subject.globus_processing?).to be nil
      end
    end

    context 'export has begun building' do
      before do
        allow(globus_export).to receive(:new?).and_return(true)
        allow(globus_export).to receive(:built?).and_return(false)
        allow(globus_export).to receive(:exported?).and_return(false)
      end

      it 'is considered in process' do
        expect(subject.globus_processing?).to be true
      end
    end

    context 'export is built but not yet available to Globus' do
      before do
        allow(globus_export).to receive(:new?).and_return(false)
        allow(globus_export).to receive(:built?).and_return(true)
        allow(globus_export).to receive(:exported?).and_return(false)
      end

      it 'is considered in process' do
        expect(subject.globus_processing?).to be true
      end
    end

    context 'export is complete & available to Globus' do
      before do
        allow(globus_export).to receive(:new?).and_return(false)
        allow(globus_export).to receive(:built?).and_return(false)
        allow(globus_export).to receive(:exported?).and_return(true)
      end

      it 'is not considered in process' do
        expect(subject.globus_processing?).to be false
      end
    end
  end

  describe '#globus_ready?' do
    context 'export has not begun building' do
      before do
        allow(subject).to receive(:globus_export).and_return(nil)
      end

      it 'is not ready' do
        expect(subject.globus_ready?).to be nil
      end
    end

    context 'export has begun building and is in process' do
      before do
        allow(globus_export).to receive(:exported?).and_return(false)
      end

      it 'is not ready' do
        expect(subject.globus_ready?).to be false
      end
    end

    context 'export is complete & available to Globus' do
      before do
        allow(globus_export).to receive(:exported?).and_return(true)
      end

      it 'is ready' do
        expect(subject.globus_ready?).to be true
      end
    end
  end

  describe '#grouped_work_presenters' do
    describe 'nested work' do
      let(:parent_doc) { SolrDocument.new('has_model_ssim' => 'Dataset') }
      let(:parent_work_presenter) { described_class.new(parent_doc, ability, request) }

      before do
        allow(subject).to receive(:in_work_presenters) { [parent_work_presenter] }
      end

      it 'has a work presenter for the datasets group' do
        expect(subject.grouped_work_presenters).to include('dataset' => [parent_work_presenter])
      end
    end
  end

  describe '#in_work_presenters' do
    describe 'nested work' do
      subject { described_class.new(SolrDocument.find(child.id), ability, request) }

      let(:parent) { create(:dataset_with_one_child) }
      let(:child) { parent.ordered_works.first }

      it 'has a work presenter' do
        expect(subject.in_work_presenters).to include(an_instance_of(described_class))
      end
    end
  end

  describe '#toplevel_rdr_collection_presenters' do
    context 'work is top-level & in one RDR collection' do
      let(:collection) { FactoryBot.build(:rdr_collection) }
      let(:work) { create(:public_dataset, member_of_collections: [collection]) }

      before { allow(subject).to receive(:toplevel_work_id) { work.id } }

      it 'returns the RDR collection presenter' do
        expect(subject.toplevel_rdr_collection_presenters.count).to eq 1
        expect(subject.toplevel_rdr_collection_presenters.first.collection_type.title).to eq 'Collection'
        expect(subject.toplevel_rdr_collection_presenters.first.title).to include(/^RDR Collection Title/)
      end
    end

    context 'work is top-level, in 2 RDR collections, & 1 generic collection' do
      let(:collection_1) { FactoryBot.build(:rdr_collection) }
      let(:collection_2) { FactoryBot.build(:rdr_collection) }
      let(:collection_3) { FactoryBot.build(:public_collection_lw) }
      let(:work) { create(:public_dataset, member_of_collections: [collection_1, collection_2, collection_3]) }

      before { allow(subject).to receive(:toplevel_work_id) { work.id } }

      it 'returns both RDR collection presenters, no others' do
        expect(subject.toplevel_rdr_collection_presenters.count).to eq 2
      end
    end

    context 'work is nested & its top-level ancestor is in an RDR collection' do
      let(:collection) { FactoryBot.build(:rdr_collection) }
      let(:parent) { create(:dataset_with_one_child, member_of_collections: [collection]) }
      let(:work) { parent.ordered_works.first }

      before { allow(subject).to receive(:toplevel_work_id) { parent.id } }

      it "returns the top-level ancestor's RDR collection presenter" do
        expect(subject.toplevel_rdr_collection_presenters.first.collection_type.title).to eq 'Collection'
        expect(subject.toplevel_rdr_collection_presenters.first.title).to include(/^RDR Collection Title/)
      end
    end

    context 'work is nested & its top level work is unknowable' do
      # happens if it or any ancestor dataset has more than one parent dataset
      let(:collection) { FactoryBot.build(:rdr_collection) }
      let(:parent) { create(:dataset_with_one_child, member_of_collections: [collection]) }
      let(:work) { parent.ordered_works.first }

      before { allow(subject).to receive(:toplevel_work_id).and_return(nil) }

      it 'returns no collection presenters' do
        expect(subject.toplevel_rdr_collection_presenters).to eq []
        expect(subject.toplevel_rdr_collection_presenters.count).to eq 0
      end
    end
  end

  describe '#ancestor_trail' do
    context 'work is top-level (has no ancestors)' do
      let(:solr_document) { SolrDocument.new(id: 'abc', Rdr::Index::Fields.in_works_ids => []) }

      it 'returns an empty array' do
        expect(subject.ancestor_trail).to eq([])
      end
    end

    context 'work has multiple parent works' do
      let(:solr_document) { SolrDocument.new(id: 'abc', Rdr::Index::Fields.in_works_ids => %w[def ghi]) }

      it 'returns an empty array' do
        expect(subject.ancestor_trail).to eq([])
      end
    end

    context 'work has an ancestor with multiple parents' do
      let(:solr_document) { SolrDocument.new(id: 'abc', Rdr::Index::Fields.in_works_ids => ['def']) }
      let(:solr_document_parent) { SolrDocument.new(id: 'def', Rdr::Index::Fields.in_works_ids => %w[ghi jkl]) }

      before do
        allow(::SolrDocument).to receive(:find).with('def') { solr_document_parent }
      end

      it 'returns an empty array' do
        expect(subject.ancestor_trail).to eq([])
      end
    end

    context 'work has one ancestor' do
      let(:solr_document) { SolrDocument.new(id: 'abc', Rdr::Index::Fields.in_works_ids => ['def']) }
      let(:solr_document_parent) { SolrDocument.new(id: 'def', Rdr::Index::Fields.in_works_ids => []) }

      before do
        allow(::SolrDocument).to receive(:find).with('def').and_return(solr_document_parent)
      end

      it 'returns the ancestor document' do
        expect(subject.ancestor_trail).to eq([solr_document_parent])
      end
    end

    context 'work has a single trail of several ancestors' do
      let(:solr_document) { SolrDocument.new(id: 'abc', Rdr::Index::Fields.in_works_ids => ['def']) }
      let(:solr_document_parent) { SolrDocument.new(id: 'def', Rdr::Index::Fields.in_works_ids => ['ghi']) }
      let(:solr_document_grandparent) { SolrDocument.new(id: 'ghi', Rdr::Index::Fields.in_works_ids => ['jkl']) }
      let(:solr_document_great_grandparent) { SolrDocument.new(id: 'jkl', Rdr::Index::Fields.in_works_ids => []) }

      before do
        allow(::SolrDocument).to receive(:find).with('def').and_return(solr_document_parent)
        allow(::SolrDocument).to receive(:find).with('ghi').and_return(solr_document_grandparent)
        allow(::SolrDocument).to receive(:find).with('jkl').and_return(solr_document_great_grandparent)
      end

      it 'returns the ancestor document nodes' do
        expect(subject.ancestor_trail).to eq([solr_document_great_grandparent, solr_document_grandparent,
                                              solr_document_parent])
      end
    end
  end
  
  describe 'Presenter Factory logic' do
    let(:parent) { FactoryBot.create(:dataset_with_text_readme_file) }   
    let(:subject) { described_class.new(SolrDocument.find(parent.id), ability, request) }

    it 'checks that presenter factory class is Hyrax::RdrMemberPresenterFactory' do
      expect(subject.presenter_factory_class).to eq(Hyrax::RdrMemberPresenterFactory)
    end
  end
end
