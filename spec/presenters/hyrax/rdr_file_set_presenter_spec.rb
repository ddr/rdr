# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Hyrax::RdrFileSetPresenter do
  let(:solr_document) { SolrDocument.new(
    original_checksum_tesim: ["md5Hash"],
    digest_ssim: ['urn:sha1:hash'],
    alpha_channels_ssi: ['alpha channel']
  )}

  let(:user) { FactoryBot.create(:user) }
  let(:file_set) { FactoryBot.create(:file_set) }
  let(:work) { FactoryBot.create(:dataset, title: ['test title'], user: user) }
  let(:request) { instance_double(ActionDispatch::Request, params: { parent_id: work.id }) }

  before do
    allow_any_instance_of(described_class).to receive(:request).and_return(request)
    work.ordered_members << file_set
    work.save!
  end

  describe 'override methods' do
    it 'test updated ancestor trails to include parent dataset' do
      presenter = described_class.new(solr_document, nil)
      expect(presenter.ancestor_trail.count).to eq(1)
      expect(presenter.ancestor_trail.first.id).to eq(work.id)
    end

    it 'test lack of alpha_channels characterization' do
      presenter = described_class.new(solr_document, nil)
      metadata = presenter.characterization_metadata
      expect(metadata).not_to have_key(:alpha_channels)
      expect(metadata[:original_checksum]).to eq(['hash'])
    end
  end

  describe 'test readme methods' do
    let(:file_set) { FactoryBot.build(:file_set, title: ['My File']) }
    let(:ability) { nil }
    let(:presenter) { described_class.new(file_set, ability, request) }
    describe '#readme?' do
      context 'when the file is a plain text README file' do
        before do
          allow(presenter).to receive(:mime_type).and_return('text/plain')
          allow(presenter).to receive(:title).and_return(['my-file-readme.txt'])
          allow(presenter).to receive(:file_format).and_return('Plain text')
        end

        it 'returns true' do
          expect(presenter.readme?).to be true
        end
      end

      context 'when the file is not a plain text or README file' do
        before do
          allow(presenter).to receive(:mime_type).and_return('text/plain')
          allow(presenter).to receive(:title).and_return(['my-file.doc'])
          allow(presenter).to receive(:file_format).and_return('Plain text')
        end

        it 'returns false' do
          expect(presenter.readme?).to be false
        end
      end
    end

    describe '#markdown_readme?' do
      context 'when the file is a Markdown README file' do
        before do
          allow(presenter).to receive(:mime_type).and_return('text/plain')
          allow(presenter).to receive(:title).and_return(['my-file-readme.md'])
          allow(presenter).to receive(:file_format).and_return('Markdown, Plain text')
        end

        it 'returns true' do
          expect(presenter.markdown_readme?).to be true
          expect(presenter.readme?).to be true
        end
      end
    end
  end
end
